#include "Light.h"

class PointLight : public Light
{
public: 

	PointLight()
	{
		this->ClassID = ID_LIGHT_POINT;
	}
	
	GLfloat Attenuation;
	GLfloat SourceRadius;
	GLfloat SourceLength;

	GLfloat MaxAttenuation;
};

GLfloat PointLight::MaxAttenuation = 100.0f;
GLuint PointLight::ClassID = ID_LIGHT_POINT;