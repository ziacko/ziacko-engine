#ifndef BUFFERHANDLER_H
#define BUFFERHANDLER_H
#include <stdlib.h>
#include "VertexBuffer.h"
#include "UniformBuffer.h"


class BufferHandler
{
public:
	BufferHandler(){};
	~BufferHandler(){};

	VertexBuffer* GetVertexByName(const GLchar* Name)
	{
		return VertexBuffers[Name];
	}

	UniformBuffer* GetUniformByName(const GLchar* Name)
	{
		return UniformBuffers[Name];
	}

	void AddVertex(const GLchar* Name, VertexBuffer* Vertex)
	{
		VertexBuffers.insert(std::make_pair(Name, Vertex));
	}

	void AddUniform(const GLchar* Name, UniformBlock* Block)
	{
		UniformBuffers.insert(std::make_pair(Name, new UniformBuffer(GL_DYNAMIC_DRAW, Block)));
	}

private:

	std::map<const GLchar*, VertexBuffer*> VertexBuffers;
	//uniform buffer
	std::map<const GLchar*, UniformBuffer*> UniformBuffers;
	//pixel packing buffer
	//pixel unpacking buffer
	//frame buffer
	// index buffer
	//shader storage
	// query buffer
	//copy buffer
	//read buffer
	//write buffer
	//texture buffer
	//transform feedback buffer
	//atomic buffer
	//dispatch buffer ???

};

#endif