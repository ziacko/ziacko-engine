#ifndef BUFFER_H
#define BUFFER_H
#include "Base.h"

class Buffer
{
public:
	virtual ~Buffer()
	{
	}

	//get a list of uniform names, types, indices, offsets and sizes
	//
	Buffer()
	{

	}

	Buffer(GLuint BufferUsage, GLuint BufferTarget, GLuint BufferSize)
	{
		this->Usage = BufferUsage;
		this->Target = BufferTarget;
		this->Size = BufferSize;
		glGenBuffers(1, &GLID);
		glBindBuffer(this->Target, GLID);
	}

	virtual GLvoid InitializeBuffer(GLvoid* BufferData)
	{
		glBufferData(this->Target, this->Size, BufferData, this->Usage);
	}

	GLuint GetGLHandle()
	{
		return GLID;
	}

	virtual GLvoid Bind()
	{
		glBindBuffer(Target, GLID);
	}

	virtual GLvoid Update(GLuint Size, GLvoid* Data)
	{
		this->Bind();
	}

	virtual GLvoid StoreData(GLuint Size, GLvoid* Data)
	{
		glBufferData(Target, Size, Data, Usage);
	}

	virtual GLvoid* GetData()
	{
		return glMapBuffer(Target, Access);
	}

	virtual GLboolean UnMap()
	{
		return glUnmapBuffer(Target);
	}

	virtual GLvoid BindBase()
	{

	}

protected:
	GLuint GLID;
	GLuint Usage;
	GLuint Target;
	GLuint Access;	
	GLuint Size;
};

#endif