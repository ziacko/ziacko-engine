#ifndef GOL_MANAGER_H
#define GOL_MANAGER_H

#include <stdlib.h>
#include "Scene.h"

enum CellState
{
	Empty = 0,
	Alive,
	Dead
};

class GOL_Manager : public Scene
{
public:

	/*we need a timer (in milliseconds) for each Tick, a
	vertex array handle (to a quad) that each Cell can use,
	we also need a grid that can be able to see the neighbors of each cell.
	hopefully the Id system can help on that front
	*/
	GOL_Manager(Camera* ManagerCamera, GLuint CellProgram, GLuint GridDimension = 100, GLfloat ManagerTickDelay = 0.0f,
		GLuint CellRandomSeed = 5616516/*lololololololololol*/, GLuint ManagerProbability = 90 /*max is 100*/)
		: TickDelay(ManagerTickDelay), RandomSeed(CellRandomSeed)
	{
		//Scene::Scene(ManagerCamera, CellProgram);
		DrawCamera = ManagerCamera;
		this->ProgramGLID = CellProgram;
		Dimensions = GridDimension;

		std::srand(CellRandomSeed);
		for (GLuint CellIter = 0; CellIter < GridDimension * GridDimension; CellIter++)
		{
			GLuint NewRand = std::rand() % 100;
			Cells.push_back((CellState)(NewRand < ManagerProbability));
		}

		GenerateBuffers();
	}

	//iterate through each cell and call draw. simple
	void Draw(glm::vec2 Resolution)
	{
		//glBindBufferBase(GL_UNIFORM_BUFFER, this->DefaultSettingsGLID, this->DefaultUniformBufferGLID);

		glDrawArraysInstanced(GL_QUADS, 0, 4, Dimensions * Dimensions);
	}

	void Update(GLfloat DeltaTime)
	{
		/*check the timer, if the timer is above the delay then tick and reset the timer,
		else add delta time to it*/

		if (CurrentTickDelay < TickDelay)
		{
			CurrentTickDelay += DeltaTime;
		}
		else
		{			
			//TickOver();

			//std::vector<GLuint> StateBuffer;
			//GLuint* Test = (GLuint*)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_WRITE);

			//GLuint* Test = (GLuint*)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_WRITE);
			//glBufferData(GL_SHADER_STORAGE_BUFFER, (GOLSettingsUniforms->Dimensions * GOLSettingsUniforms->Dimensions) * sizeof(GLuint), Cells.data(), GL_DYNAMIC_DRAW);

			CurrentTickDelay = 0.0f;
		}
	}

	void CheckNode(CellState CurrentState, GLuint& NeighborCount, GLuint& DeadNeighborCount)
	{
		switch (CurrentState)
		{
			case Alive:
			{
				NeighborCount++;
				break;
			}

			case Dead:
			{
				DeadNeighborCount++;
				break; 
			}

			case Empty:
			{
				break;
			}
		}
	}

	void SetupTweakBar(TwBar* TweakBar, GLfloat& TotalTime, GLfloat& FramesPerSec)
	{
		TwAddVarRO(TweakBar, "Total Time", TwType::TW_TYPE_FLOAT, &TotalTime, "");
		TwAddVarRO(TweakBar, "FramesPerSec", TwType::TW_TYPE_FLOAT, &FramesPerSec, "");
		//TwAddVarRW(TweakBar, "Offset", TwType::TW_TYPE_FLOAT, &this->Spacing, "min=0 max=1 step=0.0001");
	}

	void TickOver()
	{
		//start the tick timer, iterate through the map to find living node and start applying the rules of GOL
		for (GLuint CellIter = 0; CellIter < Dimensions * Dimensions; CellIter++)
		{
			//get Column
			GLuint Column = CellIter % (GLuint)Dimensions;
			//get Row
			GLuint Row = (CellIter / (GLuint)Dimensions);

			if (Cells[CellIter] == Empty)
			{
				continue;
			}

			GLuint NeighborCount = 0;
			GLuint DeadNeighborCount = 0;
			GLuint Dimension = Dimensions - 1;
			//check if in the first row
			if (CellIter < Dimensions)
			{
				//check if the Item is not in the last column
				if (CellIter % Dimensions == 0)
				{
					//next row, next column
					CheckNode(Cells[CellIter + Dimension + 1], NeighborCount, DeadNeighborCount);
					//next row, this column
					CheckNode(Cells[CellIter + Dimension], NeighborCount, DeadNeighborCount);
					//this row, next column
					CheckNode(Cells[CellIter + 1], NeighborCount, DeadNeighborCount);
				}

				//check if its the last column in the row
				else if (CellIter % Dimension == 0)
				{
					//next row, last column
					CheckNode(Cells[(CellIter + Dimension) - 1], NeighborCount, DeadNeighborCount);
					//next row, this column
					CheckNode(Cells[CellIter + Dimension], NeighborCount, DeadNeighborCount);
					//this row, last column
					CheckNode(Cells[CellIter - 1], NeighborCount, DeadNeighborCount);
				}

				else
				{
					//5 neighbors to consider
					//this row, last column
					CheckNode(Cells[CellIter - 1], NeighborCount, DeadNeighborCount);
					//this row, next column
					CheckNode(Cells[CellIter + 1], NeighborCount, DeadNeighborCount);
					//next row, last column
					CheckNode(Cells[(CellIter + Dimension) - 1], NeighborCount, DeadNeighborCount);
					//next row, this column
					CheckNode(Cells[(CellIter + Dimension)], NeighborCount, DeadNeighborCount);
					//next row, next column
					CheckNode(Cells[(CellIter + Dimension) + 1], NeighborCount, DeadNeighborCount);
				}
			}

			//check if node is in the last row
			else if (CellIter >
				(((Dimensions * Dimensions) - Dimensions) - 1)
				&& CellIter < ((Dimensions * Dimensions) -1))
			{
				//check if the column is a multiple of the dimension (the first column)
				if ((CellIter % Dimensions) == 0)
				{
					//last row, next column
					CheckNode(Cells[(CellIter - Dimension) + 1], NeighborCount, DeadNeighborCount); //lower middle node
					//last row, this column
					CheckNode(Cells[CellIter - Dimension], NeighborCount, DeadNeighborCount); //lower middle node
					//this row, next column
					CheckNode(Cells[CellIter + 1], NeighborCount, DeadNeighborCount); //lower middle node
				}

				else if ((CellIter % Dimension == 0))
				{
					//last row, this column
					CheckNode(Cells[(CellIter - Dimension)], NeighborCount, DeadNeighborCount); //lower middle node
					//last row, last column
					CheckNode(Cells[(CellIter - Dimension) - 1], NeighborCount, DeadNeighborCount); //lower middle node
					//this row, last column
					CheckNode(Cells[CellIter - 1], NeighborCount, DeadNeighborCount); //lower middle node
				}

				else
				{
					//5 neighbors to consider
					//this row, last column
					CheckNode(Cells[CellIter - 1], NeighborCount, DeadNeighborCount);
					//this row, next column
					CheckNode(Cells[CellIter + 1], NeighborCount, DeadNeighborCount);
					//last row, last column
					CheckNode(Cells[(CellIter - Dimension) - 1], NeighborCount, DeadNeighborCount);
					//last row, this column
					CheckNode(Cells[(CellIter - Dimension)], NeighborCount, DeadNeighborCount);
					//last row, next column
					CheckNode(Cells[(CellIter - Dimension) + 1], NeighborCount, DeadNeighborCount);
				}
			}

			else 
			{
				if (CellIter < (Dimensions * Dimensions) - 1)
				{
					//8 neighbors to check
					//last row, last column
					CheckNode(Cells[(CellIter - Dimension) - 1], NeighborCount, DeadNeighborCount);
					//last row, this column
					CheckNode(Cells[(CellIter - Dimension)], NeighborCount, DeadNeighborCount);
					//last row, next column
					CheckNode(Cells[(CellIter - Dimension) + 1], NeighborCount, DeadNeighborCount);
					//this row, last column
					CheckNode(Cells[CellIter - 1], NeighborCount, DeadNeighborCount);
					//this row, next column
					CheckNode(Cells[CellIter + 1], NeighborCount, DeadNeighborCount);
					//next row, last column
					CheckNode(Cells[(CellIter + Dimension) - 1], NeighborCount, DeadNeighborCount);
					//next row, this column
					CheckNode(Cells[(CellIter + Dimension)], NeighborCount, DeadNeighborCount);
					//next row, next column
					CheckNode(Cells[(CellIter + Dimension) + 1], NeighborCount, DeadNeighborCount);
				}
			}

				if (NeighborCount < 2 && Cells[CellIter] == Alive)
				{
					Cells[CellIter] = Dead;
				}

				else if (NeighborCount >= 2 && Cells[CellIter] == Alive)
				{
					if (NeighborCount > 3)
					{
						Cells[CellIter] = Dead;
					}

					else
					{
						Cells[CellIter] = Alive;
					}
				}

				else if (NeighborCount == 3 && Cells[CellIter] == Dead)
				{
					Cells[CellIter] = Alive;
				}
			}
	}

	void GenerateBuffers()
	{

		Scene::GenerateBuffers(this->ProgramGLID, DrawCamera);
		SetupUniforms();
		//make a quad, pass it on to OpenGL and get the vertex array handle for the cells
		float CellQuad[] =
		{
			0.0f, 0.0f, 0.1f,
			1280.0f / Dimensions, 0.0f, 0.1f,
			1280.0f / Dimensions, 720.0f / Dimensions, 0.1f,
			0.0f, 720.0f / Dimensions, 0.1f
		};

		ZiackoEngine::GetBuffers()->AddVertex("GOL", new VertexBuffer(GL_STATIC_DRAW, sizeof(float) * 3 * 4, CellQuad));

		//glGenBuffers(1, &VertexBuffer);
		/*glGenBuffers(1, &GOLUniformBufferGLID);
		glGenBuffers(1, &GOLStatusBufferGLID);

		glBindBuffer(GL_UNIFORM_BUFFER, GOLUniformBufferGLID);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(GOLSettingsBuffer), GOLSettingsUniforms, GL_STATIC_DRAW);
		glBindBufferBase(GL_UNIFORM_BUFFER, this->GOLSettingsGLID, this->GOLUniformBufferGLID);

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, GOLStatusBufferGLID);
		glBufferData(GL_SHADER_STORAGE_BUFFER, (Dimensions * Dimensions) * sizeof(GLuint), Cells.data(), GL_DYNAMIC_DRAW);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, this->GOLStatusGLID, this->GOLStatusBufferGLID);*/

		/*glGenVertexArrays(1, &VertexArray);

		glBindVertexArray(VertexArray);		
		glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * 4, CellQuad, GL_STATIC_DRAW);
		
		//
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (char*)0);
		
		glBindVertexArray(VertexArray);
		
		glUseProgram(this->ProgramGLID);*/
	}

	void SetupUniforms()
	{
		//BufferHandler::GetUniformByName("DefaultSettings")->SetUniformIDFromProgram(this->ProgramGLID);
		this->DefaultSettingsGLID = glGetUniformBlockIndex(this->ProgramGLID, "DefaultSettings");
		this->GOLSettingsGLID = glGetUniformBlockIndex(this->ProgramGLID, "GOLSettings");
		this->GOLStatusGLID = glGetProgramResourceIndex(this->ProgramGLID, GL_SHADER_STORAGE_BLOCK, "GOLStatus");
	}

	void UpdateBuffers()
	{

	}

	~GOL_Manager(){};
	
	GLfloat TickDelay; // time between ticks in milliseconds
	GLfloat CurrentTickDelay; // our timer
	GLuint RandomSeed;

	GLuint ProgramGLID;

	GLuint DefaultSettingsGLID;
	GLuint GOLSettingsGLID;
	GLuint GOLStatusGLID;

	//GLuint VertexBuffer;
	//GLuint VertexArray; // essentially a handle to a quad stored in GPU memory
	GLuint DefaultUniformBufferGLID;
	GLuint GOLUniformBufferGLID;
	GLuint GOLStatusBufferGLID;

	GLuint Dimensions;

	static glm::vec4 AliveColor;
	static glm::vec4 DeadColor;
	static glm::vec4 EmptyColor;

	std::vector<CellState> Cells;
};

glm::vec4 GOL_Manager::AliveColor = glm::vec4(0, 1, 0, 1);
glm::vec4 GOL_Manager::DeadColor = glm::vec4(1, 0, 0, 1);
glm::vec4 GOL_Manager::EmptyColor = glm::vec4(0, 0, 1, 1);


#endif