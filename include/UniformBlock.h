#ifndef UNIFORMBLOCK_H
#define UNIFORMBLOCK_H
#include "Uniform.h"

class UniformBlock : public Base
{
public:

	UniformBlock(){};
	UniformBlock(const GLchar* BlockName, std::map<const GLchar*, Uniform*> BlockUniforms)
	{
		Name = BlockName;
		Uniforms = BlockUniforms;
	}
	~UniformBlock(){};

	void AddUniform(const GLchar* Name, Uniform* NewUniform)
	{
		Uniforms.insert(std::make_pair(Name, NewUniform));
	}

	std::map<const GLchar*, Uniform*> Uniforms;
	const GLchar* Name;
};

#endif