#ifndef UNIFORMBUFFER_H
#define UNIFORMBUFFER_H
#include "Buffer.h"
#include "UniformBlock.h"

class UniformBuffer : Buffer
{
public: 

	UniformBuffer(){};
	UniformBuffer(GLenum BufferUsage, UniformBlock* Block)
	{
		//get the size of the buffer from the map
		this->Block = Block;
		GLuint BufferSize = 0;
		for (auto Iter : this->Block->Uniforms)
		{
			BufferSize += Iter.second->Size;
		}
		Buffer(BufferUsage, GL_UNIFORM_BUFFER, BufferSize);
		this->UniformGLID = 0;
	}

	GLvoid SetUniformIDFromProgram(GLuint ProgramGLID)
	{
		//this->UniformGLID = 0;
		//UniformGLID = glGetUniformBlockIndex(ProgramGLID, this->Block->Name);
	}

	void BindBase() override
	{
		glBindBufferBase(this->Target, this->UniformGLID, this->GLID);
	}

protected:

	GLuint UniformGLID;
	
	UniformBlock* Block;

};


#endif