#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H
#include "Buffer.h"

class VertexBuffer : Buffer
{
public:
	//ok i want the class to automatically create a list of
	//vertex arrays on initialization
	VertexBuffer();

	VertexBuffer(unsigned int BufferUsage, unsigned int BufferSize, void* BufferData)
		: Buffer(BufferUsage, GL_ARRAY_BUFFER, BufferSize)
	{
		InitializeBuffer(BufferData);
	}

	//ad a method for adding vertex arrays to the buffer
	void AddVertexArray(unsigned int VertexSize, void* VertexData)
	{
		unsigned int NewArray = 0;
		glGenVertexArrays(1, &NewArray);
		glBindVertexArray(NewArray);
		//bind this buffer in case it hasn't already been
		Bind();
		//pass the vertex data
		glBufferData(Target, VertexSize, VertexData, Usage);

		VertexArrays.push_back(NewArray);
	}

	~VertexBuffer()
	{

	}


protected:

	std::vector<unsigned int> VertexArrays;

};

#endif