#ifndef UNIFORM_H
#define UNIFORM_H
#include "Base.h"

class Uniform : Base
{
public:

	Uniform(int UniformOffset, int UniformSize, int UniformIndex,
		int UniformType, int UniformArrayStride, int UniformMatrixStride)
	{
		Offset = UniformOffset;
		Size = UniformSize;
		Type = UniformType;
		ArrayStride = UniformArrayStride;
		MatrixStride = UniformMatrixStride; //brb
	}

	~Uniform(){};
	Uniform(){};

	int Offset;
	int Size;
	int Type;
	int ArrayStride;
	int MatrixStride;

	//name, size, type, array stride, matrix stride, 

};


#endif