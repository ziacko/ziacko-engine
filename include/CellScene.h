#ifndef CELLSCENE_H
#define CELLSCENE_H
#include "Scene.h"

class CellScene : public Scene
{
public:

	CellScene(const char* SceneName, Camera* SceneCamera, 
			GLuint SceneProgramID, GLuint SceneVAO, GLuint TextureID,
			GLfloat CellFilter = 0.3f, GLfloat CellRedMod = 0.5f, GLfloat CellGreenMod = 0.5f, 
			GLfloat CellBlueMod = 0.5f) : Scene(SceneName, SceneCamera, SceneProgramID, SceneVAO)
	{
		this->Texture = TextureID;
		this->Filter = CellFilter;
		this->RedMod = CellRedMod;
		this->GreenMod = CellGreenMod;
		this->BlueMod = CellBlueMod;
		SetupUniforms();
	}

	void Draw(glm::vec2 MousePosition)
	{
		Scene::Draw(MousePosition);
		glUniform1i(this->TextureGLID, this->Texture);
		glUniform1f(this->FilterGLID, this->Filter);
		glUniform1f(this->RedModGLID, this->RedMod);
		glUniform1f(this->GreenModGLID, this->GreenMod);
		glUniform1f(this->BlueModGLID, this->BlueMod);

		glActiveTexture(GL_TEXTURE0 + Texture);
		glBindTexture(GL_TEXTURE_2D, Texture);

		glDrawArrays(GL_QUADS, 0, 4);
	}

	void Update()
	{

	}

	void SetupUniforms()
	{
		Scene::SetupUniforms();
		this->TextureGLID = glGetUniformLocation(this->ProgramGLID, "Texture");
		this->FilterGLID = glGetUniformLocation(this->ProgramGLID, "Filter");
		this->RedModGLID = glGetUniformLocation(this->ProgramGLID, "RedMod");
		this->GreenModGLID = glGetUniformLocation(this->ProgramGLID, "GreenMod");
		this->BlueModGLID = glGetUniformLocation(this->ProgramGLID, "BlueMod");
	}

	void SetupTweakBar(TwBar* TweakBar, GLfloat& TotalTime, GLfloat& FramesPerSec, TwType ExampleType, RenderExample TempExample)
	{
		Scene::SetupTweakBar(TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
		TwAddVarRW(TweakBar, "Red Mod", TwType::TW_TYPE_FLOAT, &this->RedMod, "min=0 max=1 step=0.01");
		TwAddVarRW(TweakBar, "Blue Mod", TwType::TW_TYPE_FLOAT, &this->GreenMod, "min=0 max=1 step=0.01");
		TwAddVarRW(TweakBar, "Green Mod", TwType::TW_TYPE_FLOAT, &this->BlueMod, "min=0 max=1 step=0.01");
		TwAddVarRW(TweakBar, "Filter", TwType::TW_TYPE_FLOAT, &this->Filter, "min=0 max=1 step=0.01");
	}

	~CellScene(){};

	GLuint Texture;
	GLfloat Filter;
	GLfloat RedMod;
	GLfloat GreenMod;
	GLfloat BlueMod;

	GLuint TextureGLID;
	GLuint FilterGLID;
	GLuint RedModGLID;
	GLuint GreenModGLID;
	GLuint BlueModGLID;
};
#endif


