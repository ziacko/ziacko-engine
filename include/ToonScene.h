#ifndef TOONSCENE_H
#define TOONSCENE_H
#include "ModelScene.h"

class ToonScene : public ModelScene
{
public:

	ToonScene(const char* SceneName, Camera* SceneCamera,
		GLuint SceneProgramID, GLuint SceneVAO, GLuint SceneTexture, 
		GLfloat ToonFilter1 = 0.95f, GLfloat ToonFilter2 = 0.75f, GLfloat ToonFilter3 = 0.5f, GLfloat ToonFilter4 = 0.25f) :
		ModelScene(SceneName, SceneCamera, SceneProgramID, SceneVAO, SceneTexture)
	{
		this->Filter1 = ToonFilter1;
		this->Filter2 = ToonFilter2;
		this->Filter3 = ToonFilter3;
		this->Filter4 = ToonFilter4;
		SetupUniforms();
	}

	void Draw(glm::vec2 MousePosition, std::vector<GLuint> VAOs, TScene<float>* Model, glm::vec4* LightPosition)
	{
		for (unsigned int VertexIter = 0; VertexIter < VAOs.size(); VertexIter++)
		{
			ModelScene::SetupDraw(VAOs[VertexIter]);
			UpdateUniforms(MousePosition, VertexIter, Model, LightPosition);
			
			ModelScene::GLDraw(Model, VertexIter);
		}
	}

	void SetupUniforms() override
	{
		LightPositionGLID = glGetUniformLocation(this->ProgramGLID, "LightPosition");
		Filter1GLID = glGetUniformLocation(this->ProgramGLID, "Filter1");
		Filter2GLID = glGetUniformLocation(this->ProgramGLID, "Filter2");
		Filter3GLID = glGetUniformLocation(this->ProgramGLID, "Filter3");
		Filter4GLID = glGetUniformLocation(this->ProgramGLID, "Filter4");
		ModelScene::SetupUniforms();
	}

	void UpdateUniforms(glm::vec2 MousePosition, unsigned int MeshIndex, TScene<float>* Model, glm::vec4* LightPosition)
	{
		ModelScene::UpdateUniforms(MousePosition, MeshIndex, Model);
		//glUniform4f(this->LightPositionGLID, LightPosition->x, LightPosition->y, LightPosition->z, LightPosition->w);
		glUniform4f(this->LightPositionGLID, 1.0f, 1.0f, 1.0f, 1.0f);
		glUniform1f(this->Filter1GLID, Filter1);
		glUniform1f(this->Filter2GLID, Filter2);
		glUniform1f(this->Filter3GLID, Filter3);
		glUniform1f(this->Filter4GLID, Filter4);
	}

	GLuint LightPositionGLID;
	GLuint Filter1GLID;
	GLuint Filter2GLID;
	GLuint Filter3GLID;
	GLuint Filter4GLID;

	GLfloat Filter1;
	GLfloat Filter2;
	GLfloat Filter3;
	GLfloat Filter4;
};

#endif