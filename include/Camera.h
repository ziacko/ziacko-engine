#ifndef CAMERA_H
#define CAMERA_H

#include "Base.h"

//class need to be aligned as glm::mat4 uses 16 bits
#if defined(_MSC_VER)
__declspec(align(16))
#else 
__attribute__((aligned(16)))
#endif	

class Camera : public Base
{
public:

	enum ProjectionType
	{
		PROJECTION_PERSPECTIVE = 0,
		PROJECTION_ORTHOGRAPHIC,
	};

	Camera()
	{
		this->ClassID = ID_CAMERA;
	}

	Camera(GLfloat Width, GLfloat Height, GLfloat CameraSpeed, ProjectionType ProjType, 
		GLfloat DrawNear = 0.01f, GLfloat DrawFar = 100.0f, GLfloat FOV = 45.0f,
		glm::mat4 transform = glm::mat4(1))
	{
		Far = DrawFar;
		Near = DrawNear;
		FieldOfView = FOV;
		CurrentProjectionType = ProjType;
		Transform = transform;
		CurrentProjectionType ? View = glm::mat4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, -5, 1)
			: inverse(Transform);
		Speed = CameraSpeed;
		Resolution = glm::vec2(Width, Height);
		CurrentProjectionType ? Projection = glm::ortho(0.0f, Resolution.x, 0.0f, Resolution.y, Near, Far) :
			Projection = glm::perspective(FieldOfView, Resolution.x / Resolution.y, Near, Far);
	}

	~Camera(){}

	GLvoid Update()
	{
		//CurrentProjectionType ? this->View = inverse(this->Transform) : 
		//	glm::mat4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, -5, 1);
		if(CurrentProjectionType == PROJECTION_ORTHOGRAPHIC)
		{
			this->View[3][2] = -5;
		}

		else
		{
			this->View = glm::inverse(this->Transform);
		}	
	}

	GLvoid FreeMovement(GLfloat DeltaTime, GLfloat Speed, const GLchar Key, GLboolean MouseButtonStatus, const glm::vec4& Up = glm::vec4(0, 1, 0, 0))
	{		
		glm::vec4 Right = Transform[0];
		glm::vec4 l_Up = Transform[1];
		glm::vec4 Forward = Transform[2];
		glm::vec4 Translation = Transform[3];

		switch (Key)
		{
		case 'W':
		{
			Translation -= Forward * (DeltaTime * Speed);
			break;
		}

		case 'S':
		{
			Translation += Forward * (DeltaTime * Speed);
			break;
		}

		case 'D':
		{
			Translation += Right * (DeltaTime * Speed);
			break;
		}

		case 'A':
		{
			Translation -= Right * (DeltaTime * Speed);
			break;
		}

		case 'Q':
		{
			Translation += l_Up * (DeltaTime * Speed);
			break;
		}

		case 'E':
		{
			Translation -= l_Up * (DeltaTime * Speed);
			break;
		}
		}

		Transform[3] = Translation;

		static GLuint PreMousePos[2] = { 0 };

		if (!MouseButtonStatus)
		{
			//get mouse position and apply it to premousepos
			WindowManager::GetMousePositionInWindow("Ziyad Barakat's Portfolio", PreMousePos[0], PreMousePos[1]);
		}

		else
		{
			static GLuint MousePos[2] = { 0 };
			WindowManager::GetMousePositionInWindow("Ziyad Barakat's Portfolio", MousePos[0], MousePos[1]);
			glm::vec2 MouseDelta = glm::vec2((GLfloat)MousePos[0] - (GLfloat)PreMousePos[0], (GLfloat)MousePos[1] - (GLfloat)PreMousePos[1]);

			PreMousePos[0] = MousePos[0];
			PreMousePos[1] = MousePos[1];

			glm::mat4 RotationMatrix;// = glm::mat4(1.0f);

			if (MouseDelta.y != 0)
			{
				RotationMatrix = glm::mat4(1.0f);
				RotationMatrix = glm::rotate(RotationMatrix, -MouseDelta.y * DeltaTime, glm::vec3(Right.x, Right.y, Right.z));
				Forward = RotationMatrix * Forward;
				l_Up = RotationMatrix * l_Up;
				Right = RotationMatrix * Right;
			}

			if (MouseDelta.x != 0)
			{
				RotationMatrix = glm::mat4(1.0f);
				RotationMatrix = glm::rotate(RotationMatrix, -MouseDelta.x * DeltaTime, glm::vec3(Up.x, Up.y, Up.z));
				Forward = RotationMatrix * Forward;
				l_Up = RotationMatrix * l_Up;
				Right = RotationMatrix * Right;
			}

			Transform[0] = Right;
			Transform[1] = l_Up;
			Transform[2] = Forward;
		}
	}

	glm::mat4 GetTransform()
	{
		return Transform;
	}

	GLvoid SetTransform(glm::mat4 NewTransform)
	{
		Transform = NewTransform;
	}

	glm::mat4 GetProjection()
	{
		return Projection;
	}

	GLvoid SetProjection(glm::mat4 NewProjection)
	{
		Projection = NewProjection;
	}

	ProjectionType GetProjectionType()
	{
		return CurrentProjectionType;
	}

	GLvoid ChangeProjectionType(ProjectionType NewProjectionType)
	{
		CurrentProjectionType = NewProjectionType;

		CurrentProjectionType ? Projection = glm::ortho(0.0f, Resolution.x, 0.0f, Resolution.y, Near, Far) :
			Projection = glm::perspective(FieldOfView, Resolution.x / Resolution.y, Near, Far);
	}

	glm::mat4 GetView()
	{
		return View;
	}

	GLvoid SetView(glm::mat4 NewView)
	{
		View = NewView;
	}

	GLboolean IsXInverted()
	{
		return InvertXAxis;
	}

	GLvoid SetInvertAxis(GLboolean InvertX)
	{
		InvertXAxis = InvertX;
	}

	GLboolean IsYInverted()
	{
		return InvertYAxis;
	}

	GLvoid SetInvertYAxis(GLboolean InvertY)
	{
		InvertYAxis = InvertY;
	}

	GLfloat GetSpeed()
	{
		return Speed;
	}

	GLvoid SetSpeed(GLfloat CameraSpeed)
	{
		Speed = CameraSpeed;
	}

	GLfloat GetFieldOfView()
	{
		return FieldOfView;
	}

	GLvoid SetFieldOfView(GLfloat FOV)
	{
		FieldOfView = FOV;
		Projection = glm::ortho<GLdouble>(0, Resolution.x, 0, Resolution.y, Near, Far);
	}

	GLfloat GetMouseXSensitivity()
	{
		return XSensitivity;
	}

	GLvoid SetMouseXSensitivity(GLfloat XMouseSensitivity)
	{
		XSensitivity = XMouseSensitivity;
	}

	GLfloat GetMouseYSensitivity()
	{
		return YSensitivity;
	}

	GLvoid SetMouseYSensitivity(GLfloat YMouseSensitivity)
	{
		YSensitivity = YMouseSensitivity;
	}

	GLfloat GetNear()
	{
		return Near;
	}

	GLvoid SetNear(GLfloat CameraNear)
	{
		Near = CameraNear;
		CurrentProjectionType ? Projection = glm::ortho(0.0f, Resolution.x, 0.0f, Resolution.y, Near, Far) :
			Projection = glm::perspective(FieldOfView, Resolution.x / Resolution.y, Near, Far);
	}

	GLfloat GetFar()
	{
		return Far;
	}

	GLvoid SetFar(GLfloat CameraFar)
	{
		Far = CameraFar;
		CurrentProjectionType ? Projection = glm::ortho(0.0f, Resolution.x, 0.0f, Resolution.y, Near, Far) :
			Projection = glm::perspective(FieldOfView, Resolution.x / Resolution.y, Near, Far);
	}

	glm::vec2 GetResolution()
	{
		return Resolution;
	}

	GLvoid SetResolution(glm::vec2 NewResolution)
	{
		Resolution = NewResolution;
	}

	GLvoid* operator new(size_t I)
	{
		return _mm_malloc(I, 16);
	}

	GLvoid operator delete(void* P)
	{
		_mm_free(P);
	}

protected:

	glm::mat4 Transform;
	glm::mat4 Projection;
	glm::mat4 View;
	glm::vec2 Resolution;

	glm::vec2 PreMousePos;
	glm::vec2 MousePos;
	GLboolean InvertXAxis;
	GLboolean InvertYAxis;

	GLfloat Speed;
	GLfloat FieldOfView;
	GLfloat XSensitivity;
	GLfloat YSensitivity;
	GLfloat Near;
	GLfloat Far;

	ProjectionType CurrentProjectionType;
};
#endif
