#ifndef SCENE_H
#define SCENE_H
//#include "Camera.h"
//#include "Utilities.h"
//#include "TinyShaders.h"
//#include "AntTweakBar.h"

class Scene : public Base
{
public:

	Scene(){}
	Scene(Camera* SceneCamera,
		GLuint SceneProgramID)
	{
		DrawCamera = SceneCamera;
		ProgramGLID = SceneProgramID;
		//this->Model = glm::mat4(1.0f);
	}
	~Scene(){}

	virtual void Draw(glm::vec2 MousePosition)
	{
		//glUseProgram(this->ProgramGLID);;
	};

	virtual void Update(){};

	virtual void SetupTweakBar(TwBar* TweakBar, GLfloat& TotalTime, GLfloat& FramesPerSec, TwType ExampleType, RenderExample TempExample)
	{
		TwRemoveAllVars(TweakBar);
		TwAddVarRW(TweakBar, "CurrentExample", ExampleType, &TempExample, 0);
		TwAddVarRO(TweakBar, "Total Time", TwType::TW_TYPE_FLOAT, &TotalTime, "");
		TwAddVarRO(TweakBar, "FramesPerSec", TwType::TW_TYPE_FLOAT, &FramesPerSec, "");
	}

	/*virtual void GenerateBuffers(GLuint Progam, Camera* SceneCamera)
	{
		//first need to get offsets
		//
		//then get indices
		GLint BufferIndex = glGetUniformBlockIndex(Progam, "DefaultSettings");
		GLint BufferSize;
		glGetActiveUniformBlockiv(Progam, BufferIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &BufferSize);

		GLubyte* UniformBuffer = (GLubyte*)malloc(BufferSize);

		const GLchar* UniformNames[4] = { "Projection",
			"View", "Model", "Resolution" };

		GLuint BufferIndices[4];
		glGetUniformIndices(Progam, 4, UniformNames, BufferIndices);

		//get the size of each uniform in the block
		GLint BufferOffsets[4];
		glGetActiveUniformsiv(Progam, 4, BufferIndices, GL_UNIFORM_OFFSET, BufferOffsets);

		memcpy(UniformBuffer + BufferOffsets[0], &SceneCamera->GetProjection(), sizeof(GLfloat) * 16);
		memcpy(UniformBuffer + BufferOffsets[1], &SceneCamera->GetView(), sizeof(GLfloat) * 16);
		memcpy(UniformBuffer + BufferOffsets[2], &SceneCamera->GetTransform(), sizeof(GLfloat) * 16);
		memcpy(UniformBuffer + BufferOffsets[3], &SceneCamera->GetResolution(), sizeof(GLfloat) * 2);

		glGenBuffers(1, &DefaultUniformsBufferGLID);
		glBindBuffer(GL_UNIFORM_BUFFER, DefaultUniformsBufferGLID);
		//lets just assume that for NOW all base types use a fixed camera position. just override this function if need be
		glBufferData(GL_UNIFORM_BUFFER, BufferSize, UniformBuffer, GL_STATIC_DRAW);
		glBindBufferBase(GL_UNIFORM_BUFFER, this->DefaultUniformsGLID, this->DefaultUniformsBufferGLID);

	}*/

	Camera* GetCamera()
	{
		return DrawCamera;
	}

	//all base scenes need a pointer to a camera, shader program and models
	Camera* DrawCamera;

	//shader program Id to be used
	GLuint ProgramGLID;
	//GLuint ID; // don't need that right now

	//common uniforms that exist between all scenes
	GLuint DefaultUniformsGLID;
	GLuint DefaultUniformsBufferGLID;
	TwBar* TweakBar;
};

#endif
