#ifndef GOLSCENE_H
#define GOLSCENE_H
#include "Scene.h"

class GOLScene : public Scene
{
public:

	GOLScene(const char* SceneName, Camera* SceneCamera,
		GLuint SceneProgramID, GLuint SceneVAO) :
		Scene(SceneName, SceneCamera, SceneProgramID, SceneVAO)
	{

		SetupUniforms();
	}

	void Draw(glm::vec2 MousePosition)
	{
		Scene::Draw(MousePosition);

		glDrawArrays(GL_TRIANGLES, 0, 25 * 25); // draw count to be changed later
	}

	void Update()
	{

	}

	void SetupUniforms()
	{
		Scene::SetupUniforms();
	}

	void SetupTweakBar(TwBar* TweakBar, GLfloat& TotalTime, GLfloat& FramesPerSec, TwType ExampleType, RenderExample TempExample)
	{
		Scene::SetupTweakBar(TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
	}

	~GOLScene(){};
};


#endif