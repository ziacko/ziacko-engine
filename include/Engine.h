#ifndef ZIACKO_ENGINE
#define ZIACKO_ENGINE
//ok we need a way to store all handlers at once instead a bunch of unconnected singletons

//textures
//events
// models
// AI
// shader programs
// buffers
// 
// 
#include "Temp.h"
//#include <TinyModels.h>
#include <TinyExtender.h>
#include <TinyWindow.h>
#include <TinyShaders.h>
#include <TinyClock.h>
#include <AntTweakBar.h>
#include "Buffer.h"
#include "CameraHandler.h"
#include "BufferHandler.h"
#include "UniformHandler.h"
#include "InputHandler.h"

class ZiackoEngine
{
public:

	ZiackoEngine()
	{
	GetEngine()->WindowName = "Ziyad Barakat's Portfolio(Game of life)";
	ZiackoEngine::CurrentResolution = glm::vec2(0);
	ZiackoEngine::MousePosition = glm::vec2(0);
	}

	~ZiackoEngine()
	{

	}

	static ZiackoEngine* GetEngine()
	{
	if (ZiackoEngine::IsInitialized)
	{
		return ZiackoEngine::Instance;
	}

	ZiackoEngine::IsInitialized = GL_TRUE;
	ZiackoEngine::Instance = new ZiackoEngine();
	ZiackoEngine::Initialize();
	return ZiackoEngine::Instance;
	}

	static GLvoid Initialize()
	{
	WindowManager::Initialize();
	WindowManager::AddWindow(GetEngine()->WindowName);
	TinyClock::Initialize();
	TinyExtender::InitializeExtensions();

	GetEngine()->Inputs = new InputHandler(GetEngine()->WindowName);
	GetEngine()->Buffers = new BufferHandler();
	GetEngine()->Cameras = new CameraHandler();
	GetEngine()->Uniforms = new UniformHandler();

	WindowManager::SetWindowOnMouseMove(GetEngine()->WindowName, &HandleMouseMotion);
	WindowManager::SetWindowOnMouseButtonEvent(GetEngine()->WindowName, &HandleMouseClick);
	WindowManager::SetWindowOnKeyEvent(GetEngine()->WindowName, &HandleKeyPresses);

	TinyShaders::SetShaderBlockParseEvent(UniformHandler::GetCallback());
	TinyShaders::LoadShaderProgramsFromConfigFile("./Resources/Shaders/Shaders.txt");
	//UniformHandler::SetLoadBlockCallback(&BufferHandler::AddUniform);
	glViewport(0, 0, static_cast<GLsizei>(CurrentResolution.x), static_cast<GLsizei>(CurrentResolution.y));
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);

	}

	static CameraHandler* GetCameras()
	{
return GetEngine()->Cameras;
	}

	static BufferHandler* GetBuffers()
	{
return GetEngine()->Buffers;

	}

	static InputHandler* GetInputs()
	{
return GetEngine()->Inputs;

	}

	static glm::vec2 GetCurrentResolution()
	{
return CurrentResolution;

	}

	static glm::vec2 GetMousePosition()
	{
return MousePosition;

	}

	static void SetCurrentResolution(glm::vec2 NewResolution)
	{
CurrentResolution = NewResolution;

	}

	static void SetMousePosition(glm::vec2 NewMousePosition)
	{
MousePosition = NewMousePosition;

	}

private:

	static void HandleMouseMotion(GLuint MouseX, GLuint MouseY, GLuint ScreenMouseX, GLuint ScreenMouseY)
	{
	SetMousePosition(glm::vec2(MouseX, MouseY));
	TwMouseMotion(MouseX, MouseY);
	}

	static void HandleMouseClick(GLuint Button, GLboolean ButtonState)
	{
	switch (Button)
	{
	case MOUSE_LEFTBUTTON:
	{
		TwMouseButton((ButtonState == MOUSE_BUTTONDOWN) ? TwMouseAction::TW_MOUSE_PRESSED : TwMouseAction::TW_MOUSE_RELEASED, TwMouseButtonID::TW_MOUSE_LEFT);
		break;
	}

	case MOUSE_RIGHTBUTTON:
	{
		GetInputs()->SetButtonState(ButtonState);
		TwMouseButton((ButtonState == MOUSE_BUTTONDOWN) ? TwMouseAction::TW_MOUSE_PRESSED : TwMouseAction::TW_MOUSE_RELEASED, TwMouseButtonID::TW_MOUSE_RIGHT);
		break;
	}

	case MOUSE_MIDDLEBUTTON:
	{
		TwMouseButton((ButtonState == MOUSE_BUTTONDOWN) ? TwMouseAction::TW_MOUSE_PRESSED : TwMouseAction::TW_MOUSE_RELEASED, TwMouseButtonID::TW_MOUSE_MIDDLE);
		break;
	}
	}
	}

	static void HandleKeyPresses(GLuint Key, GLboolean KeyPressed)
	{
	if (KeyPressed)
	{
		GetInputs()->SetCurrentKey(Key);
	}

	else
	{
		GetInputs()->SetCurrentKey(0);
	}
	}

	const GLchar* WindowName;
	
	static ZiackoEngine* Instance;

	static glm::vec2 MousePosition;
	static glm::vec2 CurrentResolution;

	GLfloat FramesPerSecond;

	static GLboolean IsInitialized;
	//add texture handler system
	//add vertex array handler
	//add shader handler
	//add mesh handler
	//
	//event handler
	//renderer
	CameraHandler* Cameras;
	BufferHandler* Buffers;
	UniformHandler* Uniforms;
	InputHandler* Inputs;
};

ZiackoEngine* ZiackoEngine::Instance = nullptr;
GLboolean ZiackoEngine::IsInitialized = GL_FALSE;
glm::vec2 ZiackoEngine::MousePosition = glm::vec2(0);
glm::vec2 ZiackoEngine::CurrentResolution = glm::vec2(0);


#endif
