#ifndef INPUT_HANDLER
#define INPUT_HANDLER

class InputHandler
{
public:
	InputHandler(const GLchar* WindowName)
	{
		CurrentKey = NULL;
		MousePosition = glm::vec2(0);
		ButtonDown = false;
		//InputHandler::MouseMotion = HandleMouseMotion;
	}

	~InputHandler() {};


	GLboolean GetButton()
	{
		return ButtonDown;
	}

	GLuint GetCurrentKey()
	{
		return CurrentKey;
	}

	glm::vec2 GetMousePosition()
	{
		return MousePosition;
	}

	static OnMouseMoveEvent GetMouseMotionEvent()
	{
		return MouseMotion;
	}

	static OnMouseButtonEvent GetMouseButtonEvent()
	{
		return MouseButton;
	}

	static OnKeyEvent GetKeyEvent()
	{
		return KeyEvent;
	}

	void SetButtonState(GLboolean NewButtonState)
	{
		ButtonDown = NewButtonState;
	}

	void SetCurrentKey(GLuint NewKey)
	{
		CurrentKey = NewKey;
	}

	//static void Ste

private:
	static OnMouseMoveEvent MouseMotion;
	static OnMouseButtonEvent MouseButton;
	static OnKeyEvent KeyEvent;

	GLboolean ButtonDown;
	GLuint CurrentKey;
	glm::vec2 MousePosition;
};

OnMouseMoveEvent InputHandler::MouseMotion = nullptr;
OnMouseButtonEvent InputHandler::MouseButton = nullptr;
OnKeyEvent InputHandler::KeyEvent = nullptr;

#endif