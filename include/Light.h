#include "Base.h"

class Light : public Base
{
public:

	Light()
	{
		this->ClassID = ID_LIGHT;
	}

	glm::vec4 Position;
	glm::quat Rotation;
	glm::vec4 Scale;

	GLfloat Intensity;
	glm::vec4 LightColor;

	unsigned int ID;
	const char* String;

	static GLfloat MaxIntensity;
};

GLfloat Light::MaxIntensity = 100.0f;