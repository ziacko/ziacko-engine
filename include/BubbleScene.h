#ifndef BUBBLESCENE_H
#define BUBBLESCENE_H
#include "Scene.h"

class BubbleScene : public Scene
{
public:
	
	BubbleScene(const char* SceneName, Camera* SceneCamera,
			GLuint SceneProgramID, GLuint SceneVAO, GLuint TextureID,
			GLfloat BubbleAttenuation = 0.1f, GLfloat BubbleOffset = 0.2f) : 
		Scene(SceneName, SceneCamera, SceneProgramID, SceneVAO)
	{
		this->Texture = TextureID;
		this->Attenuation = BubbleAttenuation;
		this->Offset = BubbleOffset;
		SetupUniforms();
	}

	void Draw(glm::vec2 MousePosition, GLuint NumVerts)
	{
		Scene::Draw(MousePosition);
		glUniform1i(this->TextureGLID, this->Texture);
		glUniform1f(this->AttenuationGLID, this->Attenuation);
		glUniform1f(this->OffsetGLID, this->Offset);
		
		glActiveTexture(GL_TEXTURE0 + Texture);
		glBindTexture(GL_TEXTURE_2D, Texture);

		glDrawArrays(GL_TRIANGLES, 0, NumVerts);
	}

	void blah(GLuint fkljhfkdfjshfd, GLuint fjhdskfh);

	void Update()
	{

	}

	void SetupUniforms()
	{
		Scene::SetupUniforms();
		this->TextureGLID = glGetUniformLocation(this->ProgramGLID, "Texture");
		this->AttenuationGLID = glGetUniformLocation(this->ProgramGLID, "Attenuation");
		this->OffsetGLID = glGetUniformLocation(this->ProgramGLID, "Offset");
	}

	void SetupTweakBar(TwBar* TweakBar, GLfloat& TotalTime, GLfloat& FramesPerSec, TwType ExampleType, RenderExample TempExample)
	{
		Scene::SetupTweakBar(TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
		TwAddVarRW(TweakBar, "Attenuation", TwType::TW_TYPE_FLOAT, &this->Attenuation, "min=0 max=1 step=0.001");
		TwAddVarRW(TweakBar, "Offset", TwType::TW_TYPE_FLOAT, &this->Offset, "min=0 max=10 step=0.1");
	}

	~BubbleScene(){};

	GLfloat Texture;
	GLfloat Attenuation;
	GLfloat Offset;

	GLuint TextureGLID;
	GLuint AttenuationGLID;
	GLuint OffsetGLID;

};


void BubbleScene::blah(GLuint fkljhfkdfjshfd, GLuint fjhdskfh)
{

}
#endif
