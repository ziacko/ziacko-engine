#ifndef DAYNIGHT_H
#define DAYNIGHT_H
#include "Scene.h"

class DayNightScene : public Scene
{
public:

	DayNightScene(const char* SceneName, Camera* SceneCamera,
		GLuint SceneProgramID, GLuint SceneVAO, GLuint DayTextureID,
		GLuint NightTextureID, GLfloat DNAttenutation = 0.005f) : 
		Scene(SceneName, SceneCamera, SceneProgramID, SceneVAO)
	{
		this->Attenuation = DNAttenutation;
		this->DayTexture = DayTextureID;
		this->NightTexture = NightTextureID;

		SetupUniforms();
	}

	void Draw(glm::vec2 MousePosition)
	{
		Scene::Draw(MousePosition);
		glUniform1i(this->AttenuationGLID, this->Attenuation);
		glUniform1i(this->DayTextureGLID, this->DayTexture);
		glUniform1i(this->NightTextureGLID, this->NightTexture);

		glActiveTexture(GL_TEXTURE0 + DayTexture);
		glBindTexture(GL_TEXTURE_2D, DayTexture);

		glActiveTexture(GL_TEXTURE0 + NightTexture);
		glBindTexture(GL_TEXTURE_2D, NightTexture);

		glDrawArrays(GL_QUADS, 0, 4);
	}

	void Update()
	{

	}

	void SetupUniforms()
	{
		Scene::SetupUniforms();
		this->AttenuationGLID = glGetUniformLocation(this->ProgramGLID, "Attenuation");
		this->DayTextureGLID = glGetUniformLocation(this->ProgramGLID, "Day");
		this->NightTextureGLID = glGetUniformLocation(this->ProgramGLID, "Night");
	}

	void SetupTweakBar(TwBar* TweakBar, GLfloat& TotalTime, GLfloat& FramesPerSec, TwType ExampleType, RenderExample TempExample)
	{
		Scene::SetupTweakBar(TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
		TwAddVarRW(TweakBar, "Attenuation", TwType::TW_TYPE_FLOAT, &this->Attenuation, "min=0 max=1 step=0.0001");
	}

	~DayNightScene(){};

	GLuint DayTexture;
	GLuint NightTexture;
	GLfloat Attenuation;

	GLuint DayTextureGLID;
	GLuint NightTextureGLID;
	GLuint AttenuationGLID;
};

#endif
