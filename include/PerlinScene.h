#include "Scene.h"
#include "TinyShaders.h"

class PerlinScene : public Scene
{
public:

	PerlinScene(const char* SceneName, Camera* SceneCamera,
		GLuint SceneProgramID, GLuint SceneVAO, 
		GLfloat PerlinModValue = 289.0f, GLfloat PerlinPermuteValue = 34.0f,
		GLfloat PerlinTayInvVal = 1.79284291400159f, GLfloat PerlinFadeVal1 = 6.0f,
		GLfloat PerlinFadeVal2 = 15.0f, GLfloat PerlinFadeVal3 = 10.0f,
		GLuint PerlinNumOctaves = 4) : Scene(SceneName, SceneCamera,
		SceneProgramID, SceneVAO)
	{
		this->ModValue = PerlinModValue;
		this->PermuteValue = PerlinPermuteValue;
		this->TaylorInverse = PerlinTayInvVal;
		this->FadeVal1 = PerlinFadeVal1;
		this->FadeVal2 = PerlinFadeVal2;
		this->FadeVal3 = PerlinFadeVal3;
		this->NumOctaves = PerlinNumOctaves;

		SetupUniforms();
	}

	void Draw(glm::vec2 MousePosition, GLfloat TotalTime)
	{
		Scene::Draw(MousePosition);
		glUniform1f(this->ModValueGLID, this->ModValue);
		glUniform1f(this->PermuteValueGLID, this->PermuteValue);
		glUniform1f(this->TaylorInverseGLID, this->TaylorInverse);
		glUniform3f(this->FadeValGLID, this->FadeVal1, this->FadeVal2, this->FadeVal3);
		glUniform1i(this->NumOctavesGLID, this->NumOctaves);
		glUniform1f(this->TotalTimeGLID, TotalTime);
		glDrawArrays(GL_QUADS, 0, 4);
	}

	void Update()
	{

	}

	void SetupUniforms()
	{
		Scene::SetupUniforms();
		this->ModValueGLID = glGetUniformLocation(this->ProgramGLID, "ModValue");
		this->PermuteValueGLID = glGetUniformLocation(this->ProgramGLID, "PermuteValue");
		this->TaylorInverseGLID = glGetUniformLocation(this->ProgramGLID, "TaylorInverse");
		this->FadeValGLID = glGetUniformLocation(this->ProgramGLID, "FadeValue");
		this->NumOctavesGLID = glGetUniformLocation(this->ProgramGLID, "NumOctaves");
		this->TotalTimeGLID = glGetUniformLocation(this->ProgramGLID, "Time");
	}

	void SetupTweakBar(TwBar* TweakBar, GLfloat& TotalTime, GLfloat& FramesPerSec, TwType ExampleType, RenderExample TempExample)
	{
		Scene::SetupTweakBar(TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
		TwAddVarRW(TweakBar, "Mod Value", TwType::TW_TYPE_FLOAT, &this->ModValue, "step=0.01, min=0 max=500");
		TwAddVarRW(TweakBar, "Permute Value", TwType::TW_TYPE_FLOAT, &this->PermuteValue, "min=0 max=100 step=0.01");
		TwAddVarRW(TweakBar, "Taylor Inverse", TwType::TW_TYPE_FLOAT, &this->TaylorInverse, "min=0 max=10 step=0.001");
		TwAddVarRW(TweakBar, "Fade Value 1", TwType::TW_TYPE_FLOAT, &this->FadeVal1, "min=0 max=20");
		TwAddVarRW(TweakBar, "Fade Value 2", TwType::TW_TYPE_FLOAT, &this->FadeVal2, "min=0 max=20");
		TwAddVarRW(TweakBar, "Fade value 3", TwType::TW_TYPE_FLOAT, &this->FadeVal3, "min=0 max=20");
		TwAddVarRW(TweakBar, "Num Octaves", TwType::TW_TYPE_INT8, &this->NumOctaves, "min=0 max=25");
	}

	~PerlinScene(){};

	GLfloat ModValue;
	GLfloat PermuteValue;
	GLfloat TaylorInverse;
	GLfloat FadeVal1;
	GLfloat FadeVal2;
	GLfloat FadeVal3;
	GLuint NumOctaves;

	GLuint ModValueGLID;
	GLuint PermuteValueGLID;
	GLuint TaylorInverseGLID;
	GLuint FadeValGLID;
	GLuint NumOctavesGLID;
	GLuint TotalTimeGLID;
};