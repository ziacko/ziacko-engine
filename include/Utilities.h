#ifndef UTILITIES_H
#define UTILITIES_H
#include <FreeImage.h>

GLuint LoadTexture(const char* Texture, GLuint Format = GL_RGBA, GLuint* Width = nullptr, GLuint* Height = nullptr)
{
	FIBITMAP* BitMap = nullptr;

	FREE_IMAGE_FORMAT FIFormat = FreeImage_GetFileType(Texture, 0);

	if (FIFormat != FIF_UNKNOWN && FreeImage_FIFSupportsReading(FIFormat))
	{
		BitMap = FreeImage_Load(FIFormat, Texture);
	}

	if (BitMap == nullptr)
	{
		return 0;
	}

	if (Width != nullptr)
	{
		*Width = FreeImage_GetWidth(BitMap);
	}

	if (Height != nullptr)
	{
		*Height = FreeImage_GetHeight(BitMap);
	}

	GLuint BitsPerPix = FreeImage_GetBPP(BitMap);

	FREE_IMAGE_COLOR_TYPE ColorType = FreeImage_GetColorType(BitMap);

	if (ColorType != FIC_RGBALPHA)
	{
		FIBITMAP* NewBitMap = FreeImage_ConvertTo32Bits(BitMap);
		FreeImage_Unload(BitMap);
		BitMap = NewBitMap;
		BitsPerPix = FreeImage_GetBPP(BitMap);
		ColorType = FreeImage_GetColorType(BitMap);
	}

	BYTE* Data = FreeImage_GetBits(BitMap);

	FREE_IMAGE_TYPE ImageType = FreeImage_GetImageType(BitMap);

	GLenum Type = (ImageType == FIT_RGBF || ImageType == FIT_FLOAT) ? GL_FLOAT : GL_UNSIGNED_BYTE;

	GLuint TextureID;
	glGenTextures(1, &TextureID);
	glBindTexture(GL_TEXTURE_2D, TextureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FreeImage_GetWidth(BitMap), FreeImage_GetHeight(BitMap), 0, Format, Type, Data);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_2D, 0);

	FreeImage_Unload(BitMap);

	return TextureID;
}

#endif