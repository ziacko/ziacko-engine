#ifndef CAMERAHANDLER_H
#define CAMERAHANDLER_H
#include "Camera.h"
#include <map>

//turn class into a singleton
class CameraHandler
{
public:

	CameraHandler()
	{
	}
	~CameraHandler();

	//static void Initialize()
	//{
	//	CameraHandler::Instance = new CameraHandler();
	//}

	Camera* GetCameraByName(const GLchar* Name)
	{
		return Cameras[Name];
	}

	void AddCamera(const GLchar* Name, Camera* NewCamera)
	{
		Cameras.insert(std::make_pair(Name, NewCamera));
	}

private:

	//static CameraHandler* Get()
	//{
	//	return CameraHandler::Instance;
	//}

	//static CameraHandler* Instance;

	std::map<const GLchar*, Camera*> Cameras;
};

//CameraHandler* CameraHandler::Instance = 0;

#endif
