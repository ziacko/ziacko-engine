#include "PointLight.h"

class SpotLight : public PointLight
{
public:

	SpotLight()
	{
		this->ClassID = ID_LIGHT_SPOT;
	}

	GLfloat InnerConeAngle;
	GLfloat OuterConeAngle;

	static GLfloat MaxInnerConeAngle;
	static GLfloat MaxOuterConeAngle;
};

GLfloat SpotLight::MaxInnerConeAngle = 90.0f;
GLfloat SpotLight::MaxOuterConeAngle = 90.0f;
GLuint SpotLight::ClassID = ID_LIGHT_SPOT;
