#ifndef UNIFORMHANDLER_H
#define UNIFORMHANDLER_H
#include "UniformBlock.h"
#include "BufferHandler.h"
#include <iostream>
#include <memory>


//typedef void(*OnBlockLoaded)(const char*, UniformBlock*);
class UniformHandler
{
public:
	UniformHandler()
	{
	}

	~UniformHandler();

	void SetLoadBlockCallback(ParseBlocks NewCallBack)
	{
		BlockCallback = NewCallBack;
	}

	GLvoid AddUniforms(GLuint ProgramHandle)
	{
		GLint NumBlocks = 0;
		glGetProgramiv(ProgramHandle, GL_ACTIVE_UNIFORM_BLOCKS, &NumBlocks);

		if (NumBlocks > 0)
		{
			std::vector<const GLchar*> Names;

			for (GLint BlockIndex = 0; BlockIndex < NumBlocks; BlockIndex++)
			{
				std::map<const GLchar*, Uniform*> Uniforms;
				GLint NameLength = 0;
				glGetActiveUniformBlockiv(ProgramHandle, BlockIndex, GL_UNIFORM_BLOCK_NAME_LENGTH, &NameLength);
				GLchar BlockName[255];

				glGetActiveUniformBlockName(ProgramHandle, BlockIndex, NameLength, 0, BlockName);

				//if block name already exists then skip
				if (UniformBlocks[BlockName] == nullptr)
				{
					//get size of the block(number of elements)
					GLint BlockSize;
					glGetActiveUniformBlockiv(ProgramHandle, BlockIndex,
						GL_UNIFORM_BLOCK_DATA_SIZE, &BlockSize);

					if (BlockSize > 0)
					{
						//get the number of uniforms in the block
						GLint NumUniforms = 0;
						glGetActiveUniformBlockiv(ProgramHandle, BlockIndex,
							GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS, &NumUniforms);

						//get the indices of the uniforms in the block
						std::vector<GLint> UniformIndices;
						UniformIndices.resize(NumUniforms);
						//GLint UniformIndices;// [NumUniforms];

						glGetActiveUniformBlockiv(ProgramHandle, BlockIndex,
							GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES, &UniformIndices[0]);

						//for each uniform in the block
						for (GLint UniformIter = 0; UniformIter < NumUniforms; UniformIter++)
						{
							//index of uniform
							GLuint UniformIndex = UniformIndices[UniformIter];

							GLint UniformNameLength;
							GLint UniformOffset;
							GLint UniformSize;
							GLint UniformType;
							GLint UniformArrayStride;
							GLint UniformMatrixStride;

							GLint TempLength = 0;
							GLint TempSize = 0;
							GLenum Temp = 0;

							//get length of the uniform name
							glGetActiveUniformsiv(ProgramHandle, 1, &UniformIndex,
								GL_UNIFORM_NAME_LENGTH, &UniformNameLength);

							//std::vector<GLchar> UniformName;
							GLchar* UniformName = new GLchar[UniformNameLength];
							//UniformName.resize(UniformNameLength);
							glGetActiveUniform(ProgramHandle, UniformIndex, UniformNameLength,
								&TempLength, &TempSize, &Temp, &UniformName[0]);

							//if a uniform with the same name already exists then skip over
							//if (GetInstance()->Uniforms[UniformName] == nullptr)
							//{
							//get uniform offsets
							glGetActiveUniformsiv(ProgramHandle, 1, &UniformIndex,
								GL_UNIFORM_OFFSET, &UniformOffset);

							//get uniform size
							glGetActiveUniformsiv(ProgramHandle, 1, &UniformIndex,
								GL_UNIFORM_SIZE, &UniformSize);

							//get uniform type
							glGetActiveUniformsiv(ProgramHandle, 1, &UniformIndex,
								GL_UNIFORM_TYPE, &UniformType);

							//get array stride
							glGetActiveUniformsiv(ProgramHandle, 1, &UniformIndex,
								GL_UNIFORM_ARRAY_STRIDE, &UniformArrayStride);

							//get matrix stride
							glGetActiveUniformsiv(ProgramHandle, 1, &UniformIndex,
								GL_UNIFORM_MATRIX_STRIDE, &UniformMatrixStride);

							GLuint SizeInBytes = UniformSize * UniformTypeSize(UniformType);

							Uniforms.insert(std::make_pair(UniformName,
								new Uniform(UniformOffset, SizeInBytes, UniformIndex, UniformType, UniformArrayStride, UniformMatrixStride)));
							//}

							//UniformBlocks.emplace()
							//GetInstance()->UniformBlocks[BlockName]->Uniforms.insert(std::make_pair(UniformName, 
							//new Uniform(UniformOffset, SizeInBytes, UniformIndex, UniformType, UniformArrayStride, UniformMatrixStride)));
						}

						//GetInstance()->UniformBlocks.insert(std::make_pair(BlockName, new UniformBlock(Uniforms)));
						//if (LoadBlock != nullptr)
						//{
						//	LoadBlock(BlockName, new UniformBlock(BlockName, Uniforms));
						//}
						//ZiackoEngine::GetBuffers()->AddUniform(BlockName, new UniformBlock(BlockName, Uniforms));
					}
				}
			}
		}
	}

	static ParseBlocks GetCallback()
	{
		return BlockCallback;
	}

private:

	unsigned int GetElementsInType(unsigned int Type)
	{
		switch (Type)
		{
		case GL_FLOAT: return 1;
		case GL_FLOAT_VEC2: return 2;
		case GL_FLOAT_VEC3: return 3;
		case GL_FLOAT_VEC4: return 4;
		case GL_INT: return 1;
		case GL_INT_VEC2: return 2;
		case GL_INT_VEC3: return 3;
		case GL_INT_VEC4: return 4;
		case GL_UNSIGNED_INT: return 1;
		case GL_UNSIGNED_INT_VEC2: return 2;
		case GL_UNSIGNED_INT_VEC3: return 3;
		case GL_UNSIGNED_INT_VEC4: return 4;
		case GL_BOOL: return 1;
		case GL_BOOL_VEC2: return 2;
		case GL_BOOL_VEC3: return 3;
		case GL_BOOL_VEC4: return 4;
		case GL_FLOAT_MAT2: return 4;
		case GL_FLOAT_MAT3: return 9;
		case GL_FLOAT_MAT4: return 16;
		case GL_FLOAT_MAT2x3: return 6;
		case GL_FLOAT_MAT2x4: return 8;
		case GL_FLOAT_MAT3x2: return 6;
		case GL_FLOAT_MAT3x4: return 12;
		case GL_FLOAT_MAT4x2: return 8;
		case GL_FLOAT_MAT4x3: return 12;
		default: return 0;
		}
	}

	GLuint UniformTypeSize(GLint Type)
	{
		switch (Type)
		{
		case GL_FLOAT:
		case GL_FLOAT_VEC2:
		case GL_FLOAT_VEC3:
		case GL_FLOAT_VEC4:
		case GL_FLOAT_MAT2:
		case GL_FLOAT_MAT3:
		case GL_FLOAT_MAT4:
		case GL_FLOAT_MAT2x3:
		case GL_FLOAT_MAT2x4:
		case GL_FLOAT_MAT3x2:
		case GL_FLOAT_MAT3x4:
		case GL_FLOAT_MAT4x2:
		case GL_FLOAT_MAT4x3:
		{
			return GetElementsInType(Type) * sizeof(GLfloat);
		}

		case GL_INT:
		case GL_INT_VEC2:
		case GL_INT_VEC3:
		case GL_INT_VEC4:
		{
			return GetElementsInType(Type) * sizeof(GLint);
		}

		case GL_UNSIGNED_INT:
		case GL_UNSIGNED_INT_VEC2:
		case GL_UNSIGNED_INT_VEC3:
		case GL_UNSIGNED_INT_VEC4:
		{
			return GetElementsInType(Type) * sizeof(GLuint);
		}

		case GL_BOOL:
		case GL_BOOL_VEC2:
		case GL_BOOL_VEC3:
		case GL_BOOL_VEC4:
		{
			return GetElementsInType(Type) * sizeof(GLboolean);
		}

		default:
			return 0;
		}
	}

	static ParseBlocks BlockCallback;

	std::map<const GLchar*, UniformBlock*> UniformBlocks;
};

ParseBlocks UniformHandler::BlockCallback = nullptr;
#endif