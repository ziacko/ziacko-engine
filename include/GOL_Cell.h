#ifndef GOL_CELL_H
#define GOL_CELL_H

class GOL_Cell
{
public:

	/*ok a cell is going to need an ID, position relative to the grid,
		vertex array(make them share a static one?), a bool for dead or not.
		make the GOL manager decide whether they live or die depending on neighbors.
		also needs a handle to a shader program -_-
	*/

	//draw each cell seperately and use Position as a transform and alive bool to help judge the resulting color
	GOL_Cell(GLuint CellID, glm::vec2 CellPosition, Camera* CellCamera, GLuint CellProgram,
		GLboolean CellIsEmpty)
	{
		this->ID = CellID;
		this->Position = CellPosition;
		this->ProgramGLID = CellProgram;
		this->CurrentState = (CellState)CellIsEmpty;
		this->MyCamera = CellCamera;

		//SetupUniforms();
	}

	void Draw(glm::vec2 Resolution)
	{
		//need the vertex array and some uniforms		
		glUseProgram(this->ProgramGLID);

		glUniformMatrix4fv(this->ProjectionGLID, 1, GL_FALSE, &this->MyCamera->GetProjection()[0][0]);
		glUniformMatrix4fv(this->ViewGLID, 1, GL_FALSE, &this->MyCamera->GetView()[0][0]);
		glUniform2f(this->PositionGLID, Position.x, Position.y);
		glUniform2f(this->ResolutionGLID, Resolution.x, Resolution.y);
		//OpenGL cant send bools across so i use int instead
		
		glUniform1i(this->CurrentStateGLID, CurrentState);
		glUniform4f(this->AliveColorGLID, GOL_Cell::AliveColor.x,
			GOL_Cell::AliveColor.y, GOL_Cell::AliveColor.z,
			GOL_Cell::AliveColor.w);

		glUniform4f(this->DeadColorGLID, GOL_Cell::DeadColor.x,
			GOL_Cell::DeadColor.y, GOL_Cell::DeadColor.z,
			GOL_Cell::DeadColor.w);

		glUniform4f(this->EmptyColorGLID, GOL_Cell::EmptyColor.x,
			GOL_Cell::EmptyColor.y, GOL_Cell::EmptyColor.z,
			GOL_Cell::EmptyColor.w);

		glDrawArrays(GL_QUADS, 0, 4);
	}

	//update cell each GOL tick
	void Update()
	{
		

	}

	void SetupUniforms()
	{
		this->ProjectionGLID = glGetUniformLocation(this->ProgramGLID, "Projection");
		this->ViewGLID = glGetUniformLocation(this->ProgramGLID, "View");
		//this->ModelGLID = glGetUniformLocation(this->ProgramGLID, "Model");
		this->CurrentStateGLID = glGetUniformLocation(this->ProgramGLID, "CurrentState");
		this->PositionGLID = glGetUniformLocation(this->ProgramGLID, "CellPosition");
		this->AliveColorGLID = glGetUniformLocation(this->ProgramGLID, "AliveColor");
		this->DeadColorGLID = glGetUniformLocation(this->ProgramGLID, "DeadColor");
		this->EmptyColorGLID = glGetUniformLocation(this->ProgramGLID, "EmptyColor");
		this->ResolutionGLID = glGetUniformLocation(this->ProgramGLID, "Resolution");
	}

	GLuint ID;
	glm::vec2 Position;
	CellState CurrentState;
	Camera* MyCamera;//need a better name for this

	static glm::vec4 DeadColor;
	static glm::vec4 AliveColor;
	static glm::vec4 EmptyColor;
	static glm::vec2 Dimensions;

	GLuint ProgramGLID;
	GLuint PositionGLID;
	GLuint CurrentStateGLID;
	GLuint ProjectionGLID;
	GLuint ViewGLID;
	GLuint DeadColorGLID;
	GLuint AliveColorGLID;
	GLuint EmptyColorGLID;
	GLuint ResolutionGLID;

	//maybe i don't need this
	//GLuint ModelGLID; // essentially position wrapped in a 4x4 matrix
};

glm::vec4 GOL_Cell::DeadColor = glm::vec4(1, 0, 0, 1);
glm::vec4 GOL_Cell::AliveColor = glm::vec4(0, 1, 0, 1);
glm::vec4 GOL_Cell::EmptyColor = glm::vec4(0, 0, 1, 1);
glm::vec2 GOL_Cell::Dimensions = glm::vec2(5);
GLuint GOL_Cell::VertexArray = 0;
GLuint GOL_Cell::VertexBuffer = 0;
#endif