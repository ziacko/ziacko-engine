#ifndef MODELSCENE_H
#define MODELSCENE_H
#include "Scene.h"
#include <vector>
#include "TinyModels.h"

class ModelScene : public Scene
{
public:
	
	ModelScene(const char* SceneName, Camera* SceneCamera,
		GLuint SceneProgramID, GLuint SceneVAO, GLuint SceneTexture) : 
	Scene(SceneName, SceneCamera, SceneProgramID, SceneVAO)
	{
		SetupUniforms();
		this->Texture = SceneTexture;
	}

	virtual void Draw(glm::vec2 MousePosition, std::vector<GLuint> VAOs, TScene<float>* Model)
	{
		for(unsigned int VertexIter = 0; VertexIter < VAOs.size(); VertexIter++)
		{
			SetupDraw(VAOs[VertexIter]);
			UpdateUniforms(MousePosition, VertexIter, Model);
			GLDraw(Model, VertexIter);
		}
	}

	virtual void UpdateUniforms(glm::vec2 MousePosition, unsigned int MeshIndex, TScene<float>* Model)
	{
		glUniformMatrix4fv(this->ProjectionGLID, 1, GL_FALSE, &this->DrawCamera->GetProjection()[0][0]);
		glUniformMatrix4fv(this->ViewGLID, 1, GL_FALSE, &this->DrawCamera->GetView()[0][0]);
		glUniformMatrix4fv(this->ModelGLID, 1, GL_FALSE, Model->GetMeshByIndex(MeshIndex)->GlobalTransform);
		glUniform2f(this->ResolutionGLID, this->DrawCamera->GetResolution().x, this->DrawCamera->GetResolution().y);
		glUniform2f(this->MousePositionGLID, MousePosition.x, this->DrawCamera->GetResolution().y - MousePosition.y);
		glUniform1i(this->TextureGLID, this->Texture);
	}

	void SetupDraw(unsigned int VertexIter)
	{
		glBindVertexArray(VertexIter);
		glUseProgram(this->ProgramGLID);
	}

	void GLDraw(TScene<float>* Model, unsigned int VertexIter)
	{
		glActiveTexture(GL_TEXTURE0 + this->Texture);
		glBindTexture(GL_TEXTURE_2D, this->Texture);
		glDrawArrays(GL_TRIANGLES, 0, Model->GetMeshByIndex(VertexIter)->Vertices.size());
		//glDrawElements(GL_TRIANGLES, Model->GetMeshByIndex(VertexIter)->Indices.size(),
		//GL_UNSIGNED_INT, Model->GetMeshByIndex(VertexIter)->Indices.data());
	}

	virtual void SetupUniforms()
	{
		Scene::SetupUniforms();
		TextureGLID = glGetUniformLocation(this->ProgramGLID, "Texture");
	}

	void SetupTweakBar(TwBar* TweakBar, GLfloat& TotalTime, GLfloat& FramesPerSec, TwType ExampleType, RenderExample TempExample)
	{
		Scene::SetupTweakBar(TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
		TwAddVarRW(TweakBar, "View Right", TwType::TW_TYPE_DIR3F, &this->DrawCamera->GetView()[0], "");
		TwAddVarRW(TweakBar, "View Up", TwType::TW_TYPE_DIR3F, &this->DrawCamera->GetView()[1], "");
		TwAddVarRW(TweakBar, "View Forward", TwType::TW_TYPE_DIR3F, &this->DrawCamera->GetView()[2], "");
		TwAddVarRW(TweakBar, "View Translation", TwType::TW_TYPE_DIR3F, &this->DrawCamera->GetView()[3], "");
	}


	~ModelScene(){};

	GLuint Texture;

	GLuint TextureGLID; 
};



#endif
