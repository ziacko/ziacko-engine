#ifndef BASE_H
#define BASE_H
#include "ClassID.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
class Base
{
public:
	virtual ~Base()
	{

	}

	Base()
	{
		this->ClassID = ID_BASE;
	}

	unsigned int ClassID;
	unsigned int UniqueID;
};

#endif
