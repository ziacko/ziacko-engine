#ifndef PARALLAX_H
#define PARALLAX_H

#include "Scene.h"
#include "TinyShaders.h"

class ParallaxScene : public Scene
{
public:

	ParallaxScene(const char* SceneName, Camera* SceneCamera, 
		GLuint SceneProgramID, GLuint SceneVAO,
		GLuint DiffuseTextureID, GLuint HeightTextureID,
		GLfloat ParallaxScale = 0.1f, GLfloat ParallaxScaleBias = 0.0f,
		GLfloat ParallaxRayHeight = 0.3f) : Scene(SceneName, SceneCamera, 
		SceneProgramID, SceneVAO)
	{
		this->Scale = ParallaxScale;
		this->ScaleBias = ParallaxScaleBias;
		this->RayHeight = ParallaxRayHeight;

		this->Diffuse = DiffuseTextureID;
		this->Height = HeightTextureID;

		SetupUniforms();
	}

	~ParallaxScene(){};

	void Draw(glm::vec2 MousePosition)
	{
		Scene::Draw(MousePosition);
		glUniform1i(this->DiffuseGLID, this->Diffuse);
		glUniform1i(this->HeightGLID, this->Height);
		glUniform1f(this->ScaleGLID, this->Scale);
		glUniform1f(this->ScaleBiasGLID, this->ScaleBias);
		glUniform1f(this->RayHeightGLID, this->RayHeight);

		glActiveTexture(GL_TEXTURE0 + Diffuse);
		glBindTexture(GL_TEXTURE_2D, Diffuse);

		glActiveTexture(GL_TEXTURE0 + Height);
		glBindTexture(GL_TEXTURE_2D, Height);

		glDrawArrays(GL_QUADS, 0, 4);
	}

	void Update()
	{
		
	}

	void SetupUniforms()
	{
		Scene::SetupUniforms();
		this->DiffuseGLID = glGetUniformLocation(this->ProgramGLID, "Diffuse");
		this->HeightGLID = glGetUniformLocation(this->ProgramGLID, "Height");
		this->ScaleGLID = glGetUniformLocation(this->ProgramGLID, "ParallaxScale");
		this->ScaleBiasGLID = glGetUniformLocation(this->ProgramGLID, "ScaleBias");
		this->RayHeightGLID = glGetUniformLocation(this->ProgramGLID, "RayHeight");
	}

	void SetupTweakBar(TwBar* TweakBar, GLfloat& TotalTime, GLfloat& FramesPerSec, TwType ExampleType, RenderExample TempExample)
	{
		Scene::SetupTweakBar(TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
		TwAddVarRW(TweakBar, "Parallax Scale", TwType::TW_TYPE_FLOAT, &this->Scale, "step=0.01 min=0 max=1");
		TwAddVarRW(TweakBar, "Scale Bias", TwType::TW_TYPE_FLOAT, &this->ScaleBias, "step=0.01 min=0 max=1");
		TwAddVarRW(TweakBar, "Ray Height", TwType::TW_TYPE_FLOAT, &this->RayHeight, "step=0.01 min=0 max=1");
	}

	GLfloat Scale;
	GLfloat ScaleBias;
	GLfloat RayHeight;
	GLuint Diffuse;
	GLuint Height;

	GLuint DiffuseGLID;
	GLuint HeightGLID;
	GLuint ScaleGLID;
	GLuint ScaleBiasGLID;
	GLuint RayHeightGLID;
};

#endif
