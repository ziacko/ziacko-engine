#ifndef CLASSID_H
#define CLASSID_H

#define ID_BASE 0
#define ID_LIGHT 1
#define ID_LIGHT_POINT 2
#define ID_LIGHT_SPOT 3
#define ID_CAMERA 4
#define ID_UNIFORM 5

#endif