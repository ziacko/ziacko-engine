#version 440

layout(location = 0) in vec4 Position;
layout(location = 1) in vec2 UV;

out vec4 vPosition;
out vec2 vUV;

uniform mat4 Projection;
uniform mat4 View;
uniform mat4 Model;

uniform vec2 Resolution; 
uniform vec2 MousePosition;

uniform float Attenuation;
uniform float Offset;

void main()
{
	vPosition = Projection * View * Model * Position;
	//vec4 MousePos = vec4(MousePosition;
	vec4 MousePos =  Projection * View * Model * vec4(MousePosition.x, MousePosition.y, 0, 1);
	vec4 Res = Projection * View * Model * vec4(Resolution.x, Resolution.y, 0, 1);
	float Distance = length(MousePos.xy - vPosition.xy);

	//if the distance between the vert and mouse position is less than attenuation then push said vert away from the mouse Position
	if( Distance * 0.25 < Attenuation)
	{
		vPosition.xy += normalize(MousePos.xy - vPosition.xy) * Distance * Offset;
	}
	vUV = Position.xy / Resolution;
	gl_Position = vPosition;
}