#version 440

in vec4 vPosition;
in vec2 vUV;

out vec4 OutColor;

uniform float Attenuation;

uniform sampler2D Day;
uniform sampler2D Night;

uniform vec2 MousePosition;

void main()
{
	vec2 TextureCoord = vUV;
	vec4 Day = texture2D(Day, TextureCoord);
	vec4 Night = texture2D(Night, TextureCoord);
	
	OutColor = mix(Day, Night, length(MousePosition - gl_FragCoord.xy) * 0.005);
}