#version 440

in vec2 vUV; // the Uv of whatever texture to be used
in vec4 vPosition;

out vec4 OutColor; //what to output to

uniform float RedMod;
uniform float GreenMod;
uniform float BlueMod;

uniform float Filter; 

uniform sampler2D Texture;

uniform vec2 Resolution;

float GetBrightness(vec4 Color)
{
	//essentially get the value of the sampled texture as a single float
	//use the mod values to tweak the results
	return Color.r * RedMod + Color.g * GreenMod + Color.b * BlueMod;
}

float SobelFilter(sampler2D MyTexture)
{
	float dy = 1.0f / Resolution.y;
	float dx = 1.0f / Resolution.x;
	//image accessing pats of the textue in a 3x3 grid. there is the current pixel and the ones surrounding it

	float s00 = GetBrightness(texture(MyTexture, vUV + vec2(-dx, dy))); // access the bottom right
	float s10 = GetBrightness(texture(MyTexture, vUV + vec2(-dx, 0.0))); // access the mid right
	float s20 = GetBrightness(texture(MyTexture, vUV + vec2(-dx, 0.0))); // mid right again
	float s01 = GetBrightness(texture(MyTexture, vUV + vec2(0.0, dy))); // bottom middle
	float s21 = GetBrightness(texture(MyTexture, vUV + vec2(0.0, -dy)));//top middle
	float s02 = GetBrightness(texture(MyTexture, vUV + vec2(dx, dy))); // bottom left
	float s12 = GetBrightness(texture(MyTexture, vUV + vec2(dx, 0.0))); // mid left
	float s22 = GetBrightness(texture(MyTexture, vUV + vec2(dx, -dy))); // top left

	float Kernel1 = s00 + 2 * s10 + s20 - (s02 + 2 * s12 + s22);
	float Kernel2 = s00 + 2 * s01 + s02 - (s20 + 2 * s21 + s22);
	//get the square root of Kernel1 squared and Kernel2 squared
	return sqrt((Kernel1 * Kernel1) + (Kernel2 * Kernel2));
}

void main()
{
	//first we need to creaete a sobel filter as a means of detecting edges in out texture
	float Result = SobelFilter(Texture);
	
	//filter is an arbitrary value that you can use in order to gauge whether an edge in an image is sharp enough
	if(Result < Filter)
	{
	
		OutColor = texture2D(Texture, vUV);
	}
	
	else
	{
		//if it fails then make the edge black
		OutColor = vec4(0);
	}	
}
