#version 440

layout(location = 0) in vec4 Position;
layout(location = 1) in vec2 UV;

out vec4 vPosition;
out vec2 vUV;

uniform mat4 Projection;
uniform mat4 View;
uniform mat4 Model;

uniform float DeltaTime;

uniform vec2 Resolution;

void main()
{
	vPosition = Projection * View * Model * Position;
	vUV = Position.xy / Resolution;
	gl_Position = vPosition;
}
