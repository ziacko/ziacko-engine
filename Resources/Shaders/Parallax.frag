#version 440

uniform sampler2D Diffuse;
uniform sampler2D Height;

in vec2 vUV;
in vec4 vPosition;

out vec4 OutColor;

uniform vec2 Resolution;

uniform float ParallaxScale;
uniform float ScaleBias;
uniform float RayHeight;

uniform vec2 MousePosition;

void main()
{
	float ParallaxLimit = -length(MousePosition / Resolution);
	ParallaxLimit *= ParallaxScale - ScaleBias;

	vec2 OffsetDirection = normalize(MousePosition);
	vec2 MaxOffset = OffsetDirection * ParallaxLimit;

	float NumSamples = 100.0f;// mix(1000.0f, 10.0f, dot(MousePosition.xy, gl_FragCoord.xy));

	float StepSize = 1.0f / NumSamples;

	vec2 DX = dFdx(vUV);
	vec2 DY = dFdy(vUV);

	float CurrentRayHeight = RayHeight;
	vec2 CurrentOffset = vec2(0);
	vec2 LastOffset = vec2(0);

	float LastSampledHeight = 1.0f;
	float CurrentSampledHeight = 1.0f;

	for(float CurrentSample = 0.0f; CurrentSample < NumSamples; NumSamples)
	{
		CurrentSampledHeight = textureGrad(Height, vUV + CurrentOffset, DX, DY).a;
		if(CurrentSampledHeight > CurrentRayHeight)
		{
			float Delta1 = CurrentSampledHeight - CurrentRayHeight;
			float Delta2 = (CurrentRayHeight + StepSize) - LastSampledHeight;
			float Ratio = Delta1 / (Delta1 + Delta2);
			CurrentOffset = (Ratio) * LastOffset + (1.0f - Ratio) * CurrentOffset;
			CurrentSample = NumSamples + 1.0f;
		}

		else
		{
			CurrentSample++;
			CurrentRayHeight -= StepSize;
			LastOffset = CurrentOffset;
			CurrentOffset += StepSize * MaxOffset;
			LastSampledHeight = CurrentSampledHeight;
		}
	}

	vec2 FinalCoords = vUV + CurrentOffset;
	OutColor = texture2D(Diffuse, FinalCoords);
}