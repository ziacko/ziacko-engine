#version 450

in vec2 vUV;
in vec4 vPosition;

out vec4 OutColour;

uniform float Time;

uniform float ModValue;// = 289.0f;
uniform float PermuteVal;// = 34.0f;

uniform float TaylorVal;// = 1.79284291400159f;
//uniform float TaylorInvVal2;// = 0.85373472095314f;

uniform float FadeVal1;// = 6.0f;
uniform float FadeVal2;// = 15.0f;
uniform float FadeVal3;// = 10.0f;

uniform int NumOctaves;

uniform vec4 BaseColor;
uniform vec4 Background;

uniform sampler2D TextureTest;

vec4 ModifyValue(vec4 Value)
{
	return Value - floor(Value * (1.0f / ModValue)) * ModValue;
}

vec4 Permute(vec4 Value)
{
	return ModifyValue(((Value * PermuteVal) + 1.0f) * Value);
}

vec4 TaylorInvertSqrt(vec4 Value)
{
	return TaylorVal* Value;
}

vec2 Fade(vec2 Value)
{
	return Value * Value * Value * (Value * (Value * FadeVal1 - FadeVal2) + FadeVal3);
}

float ClassicNoise(vec2 PerlinVector)
{
	vec4 Pi = floor(PerlinVector.xyxy) + vec4(0.0, 0.0, 1.0, 1.0);
	vec4 PerlinFractal = fract(PerlinVector.xyxy) - vec4(0.0, 0.0, 1.0, 1.0);
	
	Pi = ModifyValue(Pi);
	vec4 IX = Pi.xzxz;
	vec4 IY = Pi.yyww;
	vec4 FractalX = PerlinFractal.xzxz;
	vec4 FractalY = PerlinFractal.yyww;

	vec4 I = Permute(Permute(IX) + IY);

	vec4 GX = fract(I * (1.0 / 41.0)) * 2.0 - 1.0;
	vec4 GY = abs(GX) - 0.5;
	vec4 TX = floor(GX + 0.5);
	GX = GX - TX;

	vec2 G00 = vec2(GX.x, GY.x);
	vec2 G10 = vec2(GX.y, GY.y);
	vec2 G01 = vec2(GX.z, GY.z);
	vec2 G11 = vec2(GX.w, GY.w);

	vec4 Normal = TaylorInvertSqrt(vec4(dot(G00, G00), dot(G01, G01), dot(G10, G10), dot(G11, G11)));

	G00 *= Normal.x;
	G01 *= Normal.y;
	G10 *= Normal.z;
	G11 *= Normal.w;

	float Normal00 = dot(G00, vec2(FractalX.x, FractalY.x));
	float Normal10 = dot(G10, vec2(FractalX.y, FractalY.y));
	float Normal01 = dot(G01, vec2(FractalX.z, FractalY.z));
	float Normal11 = dot(G11, vec2(FractalX.w, FractalY.w));

	vec2 FadeXY = Fade(PerlinFractal.xy);
	vec2 NormalX = mix(vec2(Normal00, Normal01), vec2(Normal10, Normal11), FadeXY.x);
	float NormalXY = mix(NormalX.x, NormalX.y, FadeXY.y);
	
	return 2.3 * NormalXY;
}

float PerlinNoise(vec2 PerlinVector, vec2 Rep)
{
	vec4 Pi = floor(PerlinVector.xyxy) + vec4(0.0, 0.0, 1.0, 1.0);
	vec4 PerlinFractal = fract(PerlinVector.xyxy) - vec4(0.0, 0.0, 1.0, 1.0);
	
	Pi = mod(Pi, Rep.xyxy);
	Pi = ModifyValue(Pi);

	vec4 IX = Pi.xzxz;
	vec4 IY = Pi.yyww;
	vec4 FractalX = PerlinFractal.xzxz;
	vec4 FractalY = PerlinFractal.yyww;

	vec4 I = Permute(Permute(IX) + IY);
	vec4 GX = fract(I * (1.0 / 41.0)) * 2.0 - 1.0;
	vec4 GY = abs(GX) - 0.5;
	vec4 TX = floor(GX + 0.5);
	GX = GX - TX;

	vec2 G00 = vec2(GX.x, GY.x);
	vec2 G10 = vec2(GX.y, GY.y);
	vec2 G01 = vec2(GX.z, GY.z);
	vec2 G11 = vec2(GX.w, GY.w);

	vec4 Normal = TaylorInvertSqrt(vec4(dot(G00, G00), dot(G10, G10), dot(G01, G01), dot(G11, G11)));

	G00 *= Normal.x;
	G01 *= Normal.y;
	G10 *= Normal.z;
	G11 *= Normal.w;

	float Normal00 = dot(G00, vec2(FractalX.x, FractalY.x));
	float Normal10 = dot(G10, vec2(FractalX.y, FractalY.y));
	float Normal01 = dot(G01, vec2(FractalX.z, FractalY.z));
	float Normal11 = dot(G11, vec2(FractalX.w, FractalY.w));

	vec2 FadeXY = Fade(PerlinFractal.xy);
	vec2 NormalX = mix(vec2(Normal00, Normal01), vec2(Normal10, Normal11), FadeXY.x);
	float NormalXY = mix(NormalX.x, NormalX.y, FadeXY.y);
	return 2.3 * NormalXY;
}

float FBM(vec2 PerlinVector, int NumOctaves, float Lacunarity, float Gain)
{
	float Sum = 0.0;
	float Amp = 1.0f;
	vec2 PP = PerlinVector;

	int OctaveIter = 0;

	for(OctaveIter = 0; OctaveIter < NumOctaves; OctaveIter++)
	{
		Amp *= Gain;
		Sum += Amp * ClassicNoise(PP);
		PP *= Lacunarity;
	}

	return Sum;
}

float Pattern(in vec2 PerlinVector)
{
	float L = 2.5;
	float G = 0.4;
	//int NumOctaves = 10;

	vec2 Q = vec2(FBM(PerlinVector + vec2(0.0, 0.0), NumOctaves, 1, G), FBM(PerlinVector + vec2(5.2, 1.3), NumOctaves, 1, G));
	vec2 R = vec2(FBM(PerlinVector + 4.0 * Q + vec2(1.7, 9.2), NumOctaves, 1, G), FBM(PerlinVector + 4.0 * Q + vec2(8.3, 2.8), NumOctaves, 1, G));

	return FBM(PerlinVector + 4.0 * R, NumOctaves, 1, G);
}

float Pattern2(in vec2 PerlinVector, out vec2 Q, out vec2 R, in float time)
{
	float L = 0.3;
	float G = 0.8;
	//int NumOctaves = 15;

	Q.x = FBM(PerlinVector + vec2(time, time), NumOctaves, L, G);
	Q.y = FBM(PerlinVector + vec2(5.2 * time, 1.3 * time), NumOctaves, L, G);
	
	R.x = FBM(PerlinVector + 4.0 * Q + vec2(1.7, 9.2), NumOctaves, L, G);
	R.y = FBM(PerlinVector + 4.0 * Q + vec2(8.3, 2.8), NumOctaves, L, G);

	return FBM(PerlinVector + 4.0 * R, NumOctaves, L, G);
}

void main()
{
	vec2 Q = vUV;
	vec2 P = -1.0 + 2.0 * Q;
	vec2 QQ;
	vec2 R;
	float Color = Pattern2(P, QQ, R, Time) * 20;

	//Color *= 3.5;

	Color = Color - int(Color);

	OutColour = texture2D(TextureTest, QQ) * vec4(Color);	
}
