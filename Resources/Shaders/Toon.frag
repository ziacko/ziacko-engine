#version 440

in vec4 vPosition;
in vec2 vUV;

out vec4 OutColor;

uniform vec2 Resolution;
uniform sampler2D Texture;
uniform vec4 LightPosition;

uniform float Filter1;
uniform float Filter2;
uniform float Filter3;
uniform float Filter4;

void main()
{
	vec4 StartColor = vec4(1);//texture2D(Texture, vUV);

	float Intensity = dot(LightPosition, normalize(vPosition));

	vec4 EndColor;

	if(Intensity > Filter1)
	{
		EndColor = vec4(1, 1, 1, 1.0);
	}

	else if(Intensity > Filter2)
	{
		EndColor = vec4(0.8, 0.8, 0.8, 1.0);
	}

	else if(Intensity > Filter3)
	{
		EndColor = vec4(0.6, 0.6, 0.6, 1.0);
	}

	else if(Intensity > Filter4)
	{
		EndColor = vec4(0.4, 0.4, 0.4, 1.0);
	}

	else
	{
		EndColor = vec4(0.2, 0.2, 0.2, 1.0);
	}

	OutColor = StartColor * EndColor;
}