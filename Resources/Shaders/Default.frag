#version 440

in vec4 vPosition;
in vec2 vUV;

out vec4 OutColor;

uniform vec2 Resolution;
uniform sampler2D Texture;

void main()
{
	OutColor = texture2D(Texture, vUV);
}