#version 440

layout(location = 0) in vec4 Position;
layout(location = 0) in vec2 UV;

out vec4 vPosition;
out vec2 vUV;
out vec4 vColor;

unsigned int NeighborCount = 0;
unsigned int DeadNeighborCount = 0;

layout(binding = 0) uniform DefaultSettings 
{
	mat4 Projection;
	mat4 View;
	mat4 Model;
	vec2 Resolution;
};

layout(packed, binding = 1) uniform GOLSettings 
{
	vec4 AliveColor;
	vec4 DeadColor;
	vec4 EmptyColor;	
	float Dimensions;
};

layout(std430, binding = 0) buffer GOLStatus 
{
	int Status[];
};

void CheckNode(unsigned int State)
{
	switch(State)
	{
		case 0:
		{
			NeighborCount++;
			break;
		}

		case 1:
		{
			DeadNeighborCount++;
			break;
		}

		case 2:
		{
			break;
		}
	}
}

void Tick()
{ 
	if (gl_InstanceID < Dimensions - 1)
	{
		//check if the Item is not in the last column
		if (mod(gl_InstanceID, (Dimensions - 1)) == 0)
		{
			//next row, next column
			CheckNode(Status[gl_InstanceID + int((Dimensions - 1)) + 1]);
			//next row, this column
			CheckNode(Status[gl_InstanceID + int((Dimensions - 1))]);
			//this row, next column
			CheckNode(Status[gl_InstanceID + 1]);
		}

		//check if its the last column in the row
		else if (mod(gl_InstanceID, (Dimensions - 1)) == 0)
		{
		//Status[gl_InstanceID]
			//next row, last column
			CheckNode(Status[(gl_InstanceID + int(Dimensions - 1)) - 1]);
			//next row, this column
			CheckNode(Status[gl_InstanceID + int(Dimensions - 1)]);
			//this row, last column
			CheckNode(Status[gl_InstanceID - 1]);
		}

		else
		{
			//5 neighbors to consider
			//this row, last column
			CheckNode(Status[gl_InstanceID - 1]);
			//this row, next column
			CheckNode(Status[gl_InstanceID + 1]);
			//next row, last column
			CheckNode(Status[(gl_InstanceID + int(Dimensions - 1)) - 1]);
			//next row, this column
			CheckNode(Status[(gl_InstanceID + int(Dimensions - 1))]);
			//next row, next column
			CheckNode(Status[(gl_InstanceID + int(Dimensions - 1)) + 1]);
		}
	}

	//check if node is in the last row
	else if (gl_InstanceID >
		(((Dimensions * Dimensions) - Dimensions) - 1)
		&& gl_InstanceID < ((Dimensions * Dimensions) -1))
	{
		//check if the column is a multiple of the dimension (the first column)
		if (mod(gl_InstanceID, Dimensions) == 0)
		{
			//last row, next column
			CheckNode(Status[(gl_InstanceID - int(Dimensions - 1)) + 1]); //lower middle node
			//last row, this column
			CheckNode(Status[gl_InstanceID - int(Dimensions - 1)]); //lower middle node
			//this row, next column
			CheckNode(Status[gl_InstanceID + 1]); //lower middle node
		}

		else if (mod(gl_InstanceID, (Dimensions - 1)) == 0)
		{
			//last row, this column
			CheckNode(Status[(gl_InstanceID - int(Dimensions - 1))]); //lower middle node
			//last row, last column
			CheckNode(Status[(gl_InstanceID - int(Dimensions - 1))]); //lower middle node
			//this row, last column
			CheckNode(Status[gl_InstanceID - 1]); //lower middle node
		}

		else
		{
			//5 neighbors to consider
			//this row, last column
			CheckNode(Status[gl_InstanceID - 1]);
			//this row, next column
			CheckNode(Status[gl_InstanceID + 1]);
			//last row, last column
			CheckNode(Status[(gl_InstanceID - int(Dimensions - 1)) - 1]);
			//last row, this column
			CheckNode(Status[(gl_InstanceID - int(Dimensions - 1))]);
			//last row, next column
			CheckNode(Status[(gl_InstanceID - int(Dimensions - 1)) + 1]);
		}
	}

	else 
	{
		if (gl_InstanceID < (Dimensions * Dimensions) - 1)
		{
			//8 neighbors to check
			//last row, last column
			CheckNode(Status[(gl_InstanceID - int(Dimensions - 1)) - 1]);
			//last row, this column
			CheckNode(Status[(gl_InstanceID - int(Dimensions - 1))]);
			//last row, next column
			CheckNode(Status[(gl_InstanceID - int(Dimensions - 1) + 1)]);
			//this row, last column
			CheckNode(Status[gl_InstanceID - 1]);
			//this row, next column
			CheckNode(Status[gl_InstanceID + 1]);
			//next row, last column
			CheckNode(Status[(gl_InstanceID + int(Dimensions - 1)) - 1]);
			//next row, this column
			CheckNode(Status[(gl_InstanceID + int(Dimensions - 1))]);
			//next row, next column
			CheckNode(Status[(gl_InstanceID + int(Dimensions - 1)) + 1]);
		}
	}

	if (NeighborCount < 2 && Status[gl_InstanceID] == 1)
	{
		Status[gl_InstanceID] = 2;
	}

	else if (NeighborCount >= 2 && Status[gl_InstanceID] == 1)
	{
		if (NeighborCount > 3)
		{
			Status[gl_InstanceID] = 2;
		}

		else
		{
			Status[gl_InstanceID] = 1;
		}
	}

	else if (NeighborCount == 3 && Status[gl_InstanceID] == 2)
	{
		Status[gl_InstanceID] = 1;
	}
}

void main()
{
	vPosition = Projection * View * Model * Position;
	vUV = UV;// / Resolution;

	float XResult = 0;
	float YResult = modf(gl_InstanceID / Dimensions.x, XResult); 
	vPosition.x = vPosition.x + (XResult * ((Resolution.x / Dimensions.x) / Resolution.x)) * 2;
	vPosition.y = vPosition.y + YResult * 2;

	/*
	need to use instanceId to figure out where in the grid the instance belongs
	instance that is less than Dimension is in the first row.
	*/
	/*if(gl_VertexID == 0)
	{
		Tick();
	}*/

	if(Status[gl_InstanceID] == 0)
	{
		vColor = EmptyColor;
	}

	if(Status[gl_InstanceID] == 1)
	{
		vColor = AliveColor;
	}

	if(Status[gl_InstanceID] == 2)
	{
		vColor = DeadColor;
	}

	gl_Position = vPosition;
}