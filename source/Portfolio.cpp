//#define GLM_SWIZZLE 1 //slows compilation far too much

#include "Engine.h"
//#include "GOL_Manager.h"

GLuint g_VAO = 0;
GLuint g_VBO = 0;
GLuint g_IBO = 0;

glm::mat4 Projection = glm::ortho<double>(0, 1280, 0, 720, 0, 1000);
glm::mat4 Position = glm::mat4(1.0f);
glm::mat4 View = glm::mat4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, -5, 1);
float QuadVerts[] = 
{
	0.0f, 0.0f, 0.1f,
	1280.0f, 0.0f, 0.1f,
	1280.0f, 720.0f, 0.1f,
	0.0f, 720.0f, 0.1f
};

TwBar* TweakBar;

glm::vec4 LightPosition = glm::vec4(1);

float FramesPerSec;

glm::vec2 MousePosition;

std::vector<unsigned int> VertexArrays;

GLuint Rocks;
GLuint Rocks_NormalHeight;
GLuint Earth_Day;
GLuint Earth_Night;
GLuint Spear_Diffuse;

GLfloat TotalTime;

TwType ExampleType;


GLuint GridVertexArray = 0;
GLuint GridVertexBuffer = 0;
GLuint GridIndexBuffer = 0;

std::vector<glm::vec4> GridVerts;

std::vector<GLuint> SceneVAOs;
GLuint SceneIBO = 0;
GLuint SceneVBO = 0;

GLuint CurrentKey = 0;
GLboolean ButtonDown = GL_FALSE;

glm::mat4 SceneProjection = glm::perspective(45.0f, 1280.0f / 720.0f, 0.1f, 1000.0f);
//TScene<float>* Scene;

glm::vec2 CurrentResolution = glm::vec2(1280, 720);

//GOL_Manager* GOL;

RenderExample CurrentExample;
RenderExample TempExample;

void CreateGrid(unsigned int Size, unsigned int Width, unsigned int Height)
{
	float VertStepSize = static_cast<float>(Width / Size);
	float HorzStepSize = static_cast<float>(Height / Size);

	for (unsigned int RowIter = 0; RowIter < Size; ++RowIter)
	{
		for (unsigned int ColumnIter = 0; ColumnIter < Size; ++ColumnIter)
		{
			//for each cell make a quad
			GridVerts.push_back(glm::vec4((VertStepSize * ColumnIter), (HorzStepSize * RowIter), 1, 1));
			GridVerts.push_back(glm::vec4((VertStepSize * ColumnIter) + VertStepSize, (HorzStepSize * RowIter), 1, 1));
			GridVerts.push_back(glm::vec4((VertStepSize * ColumnIter), (HorzStepSize * RowIter) + HorzStepSize, 1, 1));

			GridVerts.push_back(glm::vec4((VertStepSize * ColumnIter), (HorzStepSize * RowIter) + HorzStepSize, 1, 1));
			GridVerts.push_back(glm::vec4((VertStepSize * ColumnIter) + VertStepSize, (HorzStepSize * RowIter), 1, 1));

			GridVerts.push_back(glm::vec4((VertStepSize * ColumnIter) + VertStepSize, (HorzStepSize * RowIter) + HorzStepSize, 1, 1));
		}
	}
	glGenBuffers(1, &GridVertexBuffer);
	glGenVertexArrays(1, &GridVertexArray);

	glBindVertexArray(GridVertexArray);
	glBindBuffer(GL_ARRAY_BUFFER, GridVertexBuffer);

	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * GridVerts.size(), GridVerts.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, static_cast<char*>(nullptr));
	glBindVertexArray(GridVertexArray);
}

void UpdateGrid(unsigned int Size, unsigned int Width, unsigned int Height)
{
	float VertStepSize = static_cast<float>(Width / Size);
	float HorzStepSize = static_cast<float>(Height / Size);

	//need to clear the vector
	GridVerts.clear();

	for (unsigned int RowIter = 0; RowIter < Size; ++RowIter)
	{
		for (unsigned int ColumnIter = 0; ColumnIter < Size; ++ColumnIter)
		{
			//for each cell make a quad
			GridVerts.push_back(glm::vec4((VertStepSize * ColumnIter), (HorzStepSize * RowIter), 1, 1));
			GridVerts.push_back(glm::vec4((VertStepSize * ColumnIter) + VertStepSize, (HorzStepSize * RowIter), 1, 1));
			GridVerts.push_back(glm::vec4((VertStepSize * ColumnIter), (HorzStepSize * RowIter) + HorzStepSize, 1, 1));

			GridVerts.push_back(glm::vec4((VertStepSize * ColumnIter), (HorzStepSize * RowIter) + HorzStepSize, 1, 1));
			GridVerts.push_back(glm::vec4((VertStepSize * ColumnIter) + VertStepSize, (HorzStepSize * RowIter), 1, 1));

			GridVerts.push_back(glm::vec4((VertStepSize * ColumnIter) + VertStepSize, (HorzStepSize * RowIter) + HorzStepSize, 1, 1));
		}
	}

	glBindVertexArray(GridVertexArray);
	glBindBuffer(GL_ARRAY_BUFFER, GridVertexBuffer);

	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * GridVerts.size(), GridVerts.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, nullptr);
	glBindVertexArray(GridVertexArray);
}

void UpdateOpenGL(float Width, float Height)
{
	float Quad[] =
	{
		0.0f, 0.0f, 0.0f,
		Width, 0.0f, 0.0f,
		Width, Height, 0.0f,
		0.0f, Height, 0.0f

		//0.0f, 720.0f, 0.0f,
		//1280.0f, 0.0f, 0.0f,
		//1280.0f, 720.0f, 0.0f,
	};

	glBindBuffer(GL_ARRAY_BUFFER, g_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * 4, Quad, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, nullptr);
	glBindVertexArray(g_VAO);

	Projection = glm::ortho<double>(0, Width, 0, Height, 0, 1000);
	ZiackoEngine::GetCameras()->GetCameraByName("Main")->SetProjection(glm::perspective(45.0f, CurrentResolution.x / CurrentResolution.y, 0.01f, 1000.0f));
	TwWindowSize((int)Width, (int)Height);
	UpdateGrid(10, (int)Width, (int)Height);
}

void HandleWindowResize(GLuint Width, GLuint Height)
{
	CurrentResolution = glm::vec2(Width, Height);

	UpdateOpenGL((float)Width, (float)Height);
}

void HandleWindowMaximize()
{
	GLuint Width = 0;
	GLuint Height = 1;

	//WindowManager::GetWindowResolution(WindowName, Width, Height); 
	CurrentResolution.x = (float)Width;
	CurrentResolution.y = (float)Height;
	UpdateOpenGL((float)Width, (float)Height);
}

//this function uses FreeImage. due to some issues i've been having with it i switched to SOIL

void Render(RenderExample CurrentExample, float FPS)
{
	//GLuint CurrentGLProgram;
	//glViewport(0, 0, CurrentResolution.x, CurrentResolution.y);
	//Parallax->Draw(MousePosition);
	//Perlin->Draw(MousePosition, TinyClock::GetTotalTime());
	//DayNight->Draw(MousePosition);
	//Cell->Draw(MousePosition);
	//Bubble->Draw(MousePosition, GridVerts.size());
	//Model->Draw(MousePosition, SceneVAOs, Scene); 
	//Toon->Draw(MousePosition, SceneVAOs, Scene, &LightPosition);
	/*switch (CurrentExample)*/
	//GOL->Draw(glm::vec2(1280, 720));
	//TwRefreshBar(TweakBar);
	//TwDraw();

	WindowManager::WindowSwapBuffers("Ziyad Barakat's Portfolio(Game of life)");
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

/*void TW_CALL ResetTweakBar(void* TweakBar)
{
	TwRemoveAllVars((TwBar*)TweakBar);

	switch (TempExample)
	{
		case RenderExample::PARALLAX:
		{
			Parallax->SetupTweakBar((TwBar*)TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
			break;
		}

		case RenderExample::PERLIN:
		{
			Perlin->SetupTweakBar((TwBar*)TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
			break;
		}

		case RenderExample::DAYNIGHT:
		{
			DayNight->SetupTweakBar((TwBar*)TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
			break;
		}

		case RenderExample::CELL:
		{
			Cell->SetupTweakBar((TwBar*)TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
			break;
		}

		case RenderExample::BUBBLE:
		{
			Bubble->SetupTweakBar((TwBar*)TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
			break;
		}

		case RenderExample::SCENE:
		{
			Model->SetupTweakBar((TwBar*)TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);			break;
		}

		case RenderExample::TOON:
		{
			TwAddVarRW((TwBar*)TweakBar, "Current Example", ExampleType, &TempExample, 0);
			TwAddButton((TwBar*)TweakBar, "Reset TweakBar", ResetTweakBar, TweakBar, NULL);
			TwAddVarRO((TwBar*)TweakBar, "Total Time", TwType::TW_TYPE_FLOAT, &TotalTime, "");
			TwAddVarRO((TwBar*)TweakBar, "Frames Per Second", TwType::TW_TYPE_FLOAT, &FramesPerSec, "");
			TwAddVarRW((TwBar*)TweakBar, "View Right", TwType::TW_TYPE_DIR3F, &MainCamera->GetView()[0], "");
			TwAddVarRW((TwBar*)TweakBar, "View Up", TwType::TW_TYPE_DIR3F, &MainCamera->GetView()[1], "");
			TwAddVarRW((TwBar*)TweakBar, "View Forward", TwType::TW_TYPE_DIR3F, &MainCamera->GetView()[2], "");
			TwAddVarRW((TwBar*)TweakBar, "View Translation", TwType::TW_TYPE_DIR3F, &MainCamera->GetView()[3], "");
			TwAddVarRW((TwBar*)TweakBar, "Light Position", TwType::TW_TYPE_QUAT4F, &LightPosition, "");
			TwAddVarRW((TwBar*)TweakBar, "Filter 1", TwType::TW_TYPE_FLOAT, &Toon->Filter1, "min=0 max=1 step=0.01");
			TwAddVarRW((TwBar*)TweakBar, "Filter 2", TwType::TW_TYPE_FLOAT, &Toon->Filter2, "min=0 max=1 step=0.01");
			TwAddVarRW((TwBar*)TweakBar, "Filter 3", TwType::TW_TYPE_FLOAT, &Toon->Filter3, "min=0 max=1 step=0.01");
			TwAddVarRW((TwBar*)TweakBar, "Filter 4", TwType::TW_TYPE_FLOAT, &Toon->Filter4, "min=0 max=1 step=0.01");
			break;
		}
		default:
		{
			break;
		}
	}
	CurrentExample = TempExample;
}

template<typename Type>
void GenBufferFromModel(TScene<Type>* Scene)
{
	SceneVAOs = std::vector<GLuint>(Scene->GetMeshCount());

	glGenBuffers(1, &SceneVBO);
	for (unsigned int ArrayIter = 0; ArrayIter < SceneVAOs.size(); ArrayIter++)
	{
		
		glGenBuffers(1, &SceneIBO);
		glGenVertexArrays(1, &SceneVAOs[ArrayIter]);

		glBindVertexArray(SceneVAOs[ArrayIter]);

		glBindBuffer(GL_ARRAY_BUFFER, SceneVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, SceneIBO);

		glBufferData(GL_ARRAY_BUFFER, Scene->GetMeshByIndex(ArrayIter)->Vertices.size() * sizeof(TVertex<Type>), Scene->GetMeshByIndex(ArrayIter)->Vertices.data(), GL_STATIC_DRAW);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, Scene->GetMeshByIndex(ArrayIter)->Indices.size() * sizeof(unsigned int), Scene->GetMeshByIndex(ArrayIter)->Indices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(4);
		glEnableVertexAttribArray(5);
		glEnableVertexAttribArray(6);

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(TVertex<Type>), (char*)TVertex<Type>::TPositionOffset);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(TVertex<Type>), (char*)TVertex<Type>::TNormalOffset);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(TVertex<Type>), (char*)TVertex<Type>::TUVOffset);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(TVertex<Type>), (char*)TVertex<Type>::TTangentOffset);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(TVertex<Type>), (char*)TVertex<Type>::TBiNormalOffset);
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(TVertex<Type>), (char*)TVertex<Type>::TIndicesOffset);
		glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(TVertex<Type>), (char*)TVertex<Type>::TWeightsOffset);
		glBindVertexArray(ArrayIter);
	}
}
*/
void Initialize()
{
	//WindowManager::Initialize();

	//WindowManager::AddWindow(WindowName);
	//TinyClock::Initialize();
	//CameraHandler::Initialize();
	//BufferHandler::Initialize();
	
	//
	//WindowManager::SetWindowOnResize(WindowName, &HandleWindowResize);
	//WindowManager::SetWindowOnMaximized(WindowName, &HandleWindowMaximize);
	//TinyExtender::InitializeExtensions();

	//auto Example = PERLIN;

	//TwInit(TW_OPENGL, nullptr);

	//GLuint StartWidth = 0;
	//GLuint StartHeight = 0;
	//WindowManager::GetWindowResolution(WindowName, StartWidth, StartHeight);
	//TwWindowSize(StartWidth, StartHeight);
	//TweakBar = TwNewBar("TestBar");
/*

	TwEnumVal ExampleEnum[] = { { PARALLAX, "Parallax" },
	{ PERLIN, "Perlin" },
	{ DAYNIGHT, "Day / Night" },
	{ CELL, "Cell shading" },
	{ BUBBLE, "Bubble Offset" },
	{ SCENE, "3D Scene" },
	{ TOON, "Toon Shader" } };*/

	
	//SceneCamera = new Camera(CurrentResolution.x, CurrentResolution.y, 10.0f, Camera::PROJECTION_PERSPECTIVE);	
	ZiackoEngine::GetCameras()->AddCamera("Main", new Camera(CurrentResolution.x, CurrentResolution.y, 10.0f, Camera::PROJECTION_ORTHOGRAPHIC));


	//ExampleType = TwDefineEnum("Current Example", ExampleEnum, 7);

	//the shader manager doesn't actually need to be initialized
	
	

	/*
	Rocks = LoadTexture("./Resources/Textures/rocks.jpg");
	Rocks_NormalHeight = LoadTexture("./Resources/Textures/rocks_NM_height.tga", GL_BGRA);
	Earth_Day = LoadTexture("./Resources/Textures/earth_diffuse.tga", GL_BGRA);
	Earth_Night = LoadTexture("./Resources/Textures/earth_lights.tga", GL_BGRA);
	Spear_Diffuse = LoadTexture("./Resources/Textures/Soulspear_DIF.tga", GL_BGRA);

	glEnable(GL_TEXTURE_2D);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glGenVertexArrays(1, &g_VAO);
	glBindVertexArray(g_VAO);

	glGenBuffers(1, &g_VBO);

	glBindBuffer(GL_ARRAY_BUFFER, g_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * 4, QuadVerts, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);
	glBindVertexArray(g_VAO);*/

	//glViewport(0, 0, static_cast<GLsizei>(CurrentResolution.x), static_cast<GLsizei>(CurrentResolution.y));

	/*CreateGrid(25, CurrentResolution.x, CurrentResolution.y);

	Scene = new TScene<float>();
	Scene->Load("./Resources/Models/Dragon.fbx");

	GenBufferFromModel(Scene);

	Parallax = new ParallaxScene("Parallax", MainCamera, TinyShaders::GetShaderProgramByName("ParallaxProgram")->Handle, g_VAO,
		Rocks, Rocks_NormalHeight);
	Parallax->SetupTweakBar((TwBar*)TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);

	Perlin = new PerlinScene("Perlin", MainCamera, TinyShaders::GetShaderProgramByName("PerlinProgram")->Handle, g_VAO);
	Perlin->SetupTweakBar((TwBar*)TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);

	DayNight = new DayNightScene("DayNight", MainCamera, TinyShaders::GetShaderProgramByName("DayNightProgram")->Handle, g_VAO,
		Earth_Day, Earth_Night);
	DayNight->SetupTweakBar((TwBar*)TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);

	Cell = new CellScene("Cell", MainCamera, TinyShaders::GetShaderProgramByName("CellProgram")->Handle, g_VAO, Earth_Day); 
	Cell->SetupTweakBar((TwBar*)TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
	
	Bubble = new BubbleScene("Bubble", MainCamera,TinyShaders::GetShaderProgramByName("BubbleProgram")->Handle, GridVertexArray, Earth_Day);
	Bubble->SetupTweakBar((TwBar*)TweakBar, TotalTime, FramesPerSec, ExampleType, TempExample);
	
	Model = new ModelScene("Model", SceneCamera, TinyShaders::GetShaderProgramByName("SceneProgram")->Handle, g_VAO, Spear_Diffuse); 	

	Toon = new ToonScene("Toon", SceneCamera, TinyShaders::GetShaderProgramByName("ToonProgram")->Handle, g_VAO, Spear_Diffuse);*/

	//GOL = new GOLScene("Game Of Life", SceneCamera, TinyShaders::GetShaderProgramByName("GOLProgram")->Handle, GridVertexArray);
	//GOL = new GOL_Manager(ZiackoEngine::GetCameras()->GetCameraByName("Main"), TinyShaders::GetShaderProgramByName("GOLProgram")->Handle);
	//GOL->SetupTweakBar(static_cast<TwBar*>(TweakBar), TotalTime, FramesPerSec);
}

int main()
{
	Initialize();

	while(!WindowManager::GetWindowShouldClose("Ziyad Barakat's Portfolio(Game of life)"))
	{
		WindowManager::PollForEvents();
		
		TinyClock::UpdateClockAdaptive();
		TotalTime = static_cast<float>(TinyClock::GetTotalTime());
		FramesPerSec = static_cast<float>(1.0f / TinyClock::GetDeltaTime());

		ZiackoEngine::GetCameras()->GetCameraByName("Main")->Update();
		//SceneCamera->Update();

		//GOL->Update(static_cast<float>(TinyClock::GetDeltaTime()));
		//GOL->Draw();

		//MainCamera->FreeMovement(TinyClock::GetDeltaTime(),
		//	1.0f, CurrentKey, ButtonDown);

		//SceneCamera->FreeMovement(TinyClock::GetDeltaTime(),
				//1.0f, CurrentKey, ButtonDown);

		Render(CurrentExample, FramesPerSec);
	}
	TwTerminate();
	TinyShaders::Shutdown();
	WindowManager::ShutDown();

	return 0;
}
